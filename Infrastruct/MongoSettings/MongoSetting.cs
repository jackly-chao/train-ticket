﻿namespace Infrastruct.MongoSettings
{
    /// <summary>
    /// Mongo配置
    /// </summary>
    public abstract class MongoSetting
    {
        public MongoSetting(string connectionString, string dbName = "0")
        {
            ConnectionString = connectionString;
            DbName = dbName;
        }

        /// <summary>
        /// 连接串
        /// </summary>
        public string ConnectionString { get; protected set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DbName { get; protected set; } = "0";
    }
}
