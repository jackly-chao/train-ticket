﻿namespace Infrastruct.MongoSettings
{
    /// <summary>
    /// 默认Mongo配置
    /// </summary>
    public class DefaultMongoSetting : MongoSetting
    {
        public DefaultMongoSetting(string connectionString, string dbName = "0") : base(connectionString, dbName)
        {

        }
    }
}
