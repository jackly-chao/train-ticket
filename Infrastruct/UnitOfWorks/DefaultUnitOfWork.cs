﻿using Domain.IUnitOfWorks;
using Infrastruct.DbContexts;

namespace Infrastruct.UnitOfWorks
{
    /// <summary>
    /// 默认工作单元
    /// </summary>
    public class DefaultUnitOfWork : UnitOfWork<DefaultDbContext>, IDefaultUnitOfWork
    {
        public DefaultUnitOfWork(DefaultDbContext dbContext) : base(dbContext)
        {

        }
    }
}
