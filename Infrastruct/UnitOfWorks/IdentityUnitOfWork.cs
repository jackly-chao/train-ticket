﻿using Domain.IUnitOfWorks;
using Infrastruct.DbContexts;

namespace Infrastruct.UnitOfWorks
{
    /// <summary>
    /// 认证工作单元
    /// </summary>
    public class IdentityUnitOfWork : UnitOfWork<IdentityDbContext>, IIdentityUnitOfWork
    {
        public IdentityUnitOfWork(IdentityDbContext dbContext) : base(dbContext)
        {

        }
    }
}
