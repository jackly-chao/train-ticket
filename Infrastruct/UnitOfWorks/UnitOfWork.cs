﻿using Domain.IUnitOfWorks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Infrastruct.UnitOfWorks
{
    /// <summary>
    /// 工作单元,这里的提交才会将数据更新到数据库,之所以加这个,是考虑到跨库/表事务
    /// </summary>
    /// <typeparam name="TDbContext">泛型数据库上下文</typeparam>
    public abstract class UnitOfWork<TDbContext> : IUnitOfWork where TDbContext : DbContext
    {
        /// <summary>
        /// 数据库上下文
        /// </summary>
        protected readonly TDbContext _db;

        public UnitOfWork(TDbContext dbContext)
        {
            _db = dbContext;
        }

        #region 同步方法
        public int Commit()
        {
            return _db.SaveChanges();
        }

        public int CommitByTransaction()
        {
            var result = 0;
            using (var transaction = _db.Database.BeginTransaction())
            {
                try
                {
                    result = _db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    result = 0;
                    transaction.Rollback();
                }
            }
            return result;
        }
        #endregion

        #region 异步方法
        public async Task<int> CommitAsync()
        {
            return await _db.SaveChangesAsync();
        }

        public async Task<int> CommitByTransactionAsync()
        {
            var result = 0;
            using (var transaction = _db.Database.BeginTransaction())
            {
                try
                {
                    result = await _db.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    result = 0;
                    transaction.Rollback();
                }
            }
            return result;
        }
        #endregion

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
