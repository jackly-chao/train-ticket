﻿using Domain.IRabbitMQs;
using Infrastruct.RabbitMQSettings;

namespace Infrastruct.RabbitMQs
{
    /// <summary>
    /// 默认RabbitMQ
    /// </summary>
    public class DefaultRabbitMQ : RabbitMQ<DefaultRabbitMQSetting>, IDefaultRabbitMQ
    {
        public DefaultRabbitMQ(DefaultRabbitMQSetting rabbitMQSetting) : base(rabbitMQSetting)
        {

        }
    }
}
