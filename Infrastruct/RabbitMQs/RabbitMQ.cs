﻿using Domain.IRabbitMQs;
using Infrastruct.RabbitMQSettings;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Tasks;

namespace Infrastruct.RabbitMQs
{
    /// <summary>
    /// RabbitMQ
    /// </summary>
    /// <typeparam name="TRabbitMQSetting">泛型RabbitMQ配置</typeparam>
    public abstract class RabbitMQ<TRabbitMQSetting> : IRabbitMQ where TRabbitMQSetting : RabbitMQSetting
    {
        /// <summary>
        /// RabbitMQ配置
        /// </summary>
        protected readonly TRabbitMQSetting _rabbitMQSetting;

        /// <summary>
        /// 通道
        /// </summary>
        protected readonly IModel _channel;

        /// <summary>
        /// 连接
        /// </summary>
        protected readonly IConnection _connection;

        public RabbitMQ(TRabbitMQSetting rabbitMQSetting)
        {
            _rabbitMQSetting = rabbitMQSetting;
            var factory = new ConnectionFactory { HostName = rabbitMQSetting.HostName, VirtualHost = rabbitMQSetting.VirtualHost, UserName = rabbitMQSetting.UserName, Password = rabbitMQSetting.Password };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        #region 同步方法
        public void SendMessage<T>(T message, string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true)
        {
            queueName ??= _rabbitMQSetting.QueueName;
            exchangeName ??= _rabbitMQSetting.ExchangeName;
            routingKey ??= _rabbitMQSetting.RoutingKey;
            _channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, durable);//声明交换机
            _channel.QueueDeclare(queueName, durable, false, false);//声明队列
            _channel.QueueBind(queueName, exchangeName, routingKey);//根据路由Key将队列与交换机进行绑定
            var basicProperties = _channel.CreateBasicProperties();
            if (durable) basicProperties.DeliveryMode = 2;
            else basicProperties.DeliveryMode = 1;
            _channel.BasicPublish(exchangeName, routingKey, true, basicProperties, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
        }

        public T ConsumeMessage<T>(string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true, bool autoAck = false)
        {
            var result = default(T);
            queueName ??= _rabbitMQSetting.QueueName;
            exchangeName ??= _rabbitMQSetting.ExchangeName;
            routingKey ??= _rabbitMQSetting.RoutingKey;
            _channel.QueueDeclare(queueName, durable, false, false);//声明队列
            _channel.QueueBind(queueName, exchangeName, routingKey);//根据路由Key将队列与交换机进行绑定
            var consumer = new EventingBasicConsumer(_channel);//创建消费者
            _channel.BasicConsume(queueName, autoAck, consumer);//消费者开启监听
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                if (body.Length > 0)
                {
                    result = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(body));
                    if (!autoAck) _channel.BasicAck(ea.DeliveryTag, false);
                    //consumer.Received要等一会儿才能拿到消息，不太符合直接返回消息的应用场景，可以考虑传入一个委托，然后在拿到消息以后执行委托（有点类似于后台定时执行任务）
                }
            };
            return result;
        }

        public T GetMessage<T>(string queueName = null, bool autoAck = false)
        {
            var result = default(T);
            queueName ??= _rabbitMQSetting.QueueName;
            var ea = _channel.BasicGet(queueName, autoAck);
            var body = ea.Body.ToArray();
            if (body.Length > 0)
            {
                result = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(body));
                if (!autoAck) _channel.BasicAck(ea.DeliveryTag, false);
            }
            return result;
        }
        #endregion

        #region 异步方法
        public async Task SendMessageAsync<T>(T message, string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true)
        {
            queueName ??= _rabbitMQSetting.QueueName;
            exchangeName ??= _rabbitMQSetting.ExchangeName;
            routingKey ??= _rabbitMQSetting.RoutingKey;
            _channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, durable);//声明交换机
            _channel.QueueDeclare(queueName, durable, false, false);//声明队列
            _channel.QueueBind(queueName, exchangeName, routingKey);//根据路由Key将队列与交换机进行绑定
            var basicProperties = _channel.CreateBasicProperties();
            if (durable) basicProperties.DeliveryMode = 2;
            else basicProperties.DeliveryMode = 1;
            _channel.BasicPublish(exchangeName, routingKey, true, basicProperties, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
            await Task.CompletedTask;
        }

        public async Task<T> ConsumeMessageAsync<T>(string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true, bool autoAck = false)
        {
            var result = default(T);
            queueName ??= _rabbitMQSetting.QueueName;
            exchangeName ??= _rabbitMQSetting.ExchangeName;
            routingKey ??= _rabbitMQSetting.RoutingKey;
            _channel.QueueDeclare(queueName, durable, false, false);//声明队列
            _channel.QueueBind(queueName, exchangeName, routingKey);//根据路由Key将队列与交换机进行绑定
            var consumer = new EventingBasicConsumer(_channel);//创建消费者
            _channel.BasicConsume(queueName, autoAck, consumer);//消费者开启监听
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                if (body.Length > 0)
                {
                    result = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(body));
                    if (!autoAck) _channel.BasicAck(ea.DeliveryTag, false);
                    //consumer.Received要等一会儿才能拿到消息，不太符合直接返回消息的应用场景，可以考虑传入一个委托，然后在拿到消息以后执行委托（有点类似于后台定时执行任务）
                }
            };
            await Task.CompletedTask;
            return result;
        }

        public async Task<T> GetMessageAsync<T>(string queueName = null, bool autoAck = false)
        {
            var result = default(T);
            queueName ??= _rabbitMQSetting.QueueName;
            var ea = _channel.BasicGet(queueName, autoAck);
            var body = ea.Body.ToArray();
            if (body.Length > 0)
            {
                result = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(body));
                if (!autoAck) _channel.BasicAck(ea.DeliveryTag, false);
            }
            await Task.CompletedTask;
            return result;
        }
        #endregion

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }
    }
}
