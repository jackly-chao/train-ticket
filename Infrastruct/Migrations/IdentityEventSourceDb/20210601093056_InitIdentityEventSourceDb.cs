﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastruct.Migrations.IdentityEventSourceDb
{
    public partial class InitIdentityEventSourceDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IdentityEventSource",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AggregateId = table.Column<int>(type: "int", nullable: false),
                    EventModelName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EventModelData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Operator = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityEventSource", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IdentityEventSource");
        }
    }
}
