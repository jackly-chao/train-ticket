﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastruct.Migrations.DefaultDb
{
    public partial class DefaultDb0624 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Method",
                table: "Permission",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Method",
                table: "Permission");
        }
    }
}
