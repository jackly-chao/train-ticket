﻿namespace Infrastruct.RabbitMQSettings
{
    /// <summary>
    /// RabbitMQ设置
    /// </summary>
    public abstract class RabbitMQSetting
    {
        public RabbitMQSetting(string hostName, string virtualHost, string userName, string password, string exchangeName = "defaltExchange", string routingKey = "defaltRoutingKey", string queueName = "defaltQueue")
        {
            HostName = hostName;
            VirtualHost = virtualHost ?? "/";
            UserName = userName ?? "guest";
            Password = password ?? "guest";
            ExchangeName = exchangeName ?? "defaltExchange";
            RoutingKey = routingKey ?? "defaltRoutingKey";
            QueueName = queueName ?? "defaltQueue";
        }

        /// <summary>
        /// 主机名，其实就是IP地址
        /// </summary>
        public string HostName { get; protected set; }

        /// <summary>
        /// 虚拟主机，类似于路由
        /// </summary>
        public string VirtualHost { get; protected set; } = "/";

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; protected set; } = "guest";

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; protected set; } = "guest";

        /// <summary>
        /// 交换机名
        /// </summary>
        public string ExchangeName { get; protected set; } = "defaltExchange";

        /// <summary>
        /// 路由Key，用于交换机和队列（或交换机和交换机）之间的绑定
        /// </summary>
        public string RoutingKey { get; protected set; } = "defaltRoutingKey";

        /// <summary>
        /// 队列名
        /// </summary>
        public string QueueName { get; protected set; } = "defaltQueue";
    }
}
