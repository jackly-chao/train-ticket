﻿namespace Infrastruct.RabbitMQSettings
{
    /// <summary>
    /// 默认RabbitMQ配置
    /// </summary>
    public class DefaultRabbitMQSetting : RabbitMQSetting
    {
        public DefaultRabbitMQSetting(string hostName, string virtualHost, string userName, string password, string exchangeName = "defaltExchange", string routingKey = "defaltRoutingKey", string queueName = "defaltQueue") : base(hostName, virtualHost, userName, password, exchangeName, routingKey, queueName)
        {

        }
    }
}
