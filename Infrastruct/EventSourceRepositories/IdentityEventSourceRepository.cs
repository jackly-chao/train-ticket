﻿using Domain.Events.Models;
using Domain.EventSources.Handlers;
using Domain.EventSources.Models;
using Infrastruct.EventSourceDbContexts;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Infrastruct.EventSourceRepositories
{
    /// <summary>
    /// 认证事件溯源仓储
    /// </summary>
    public class IdentityEventSourceRepository : EventSourceRepository<IdentityEventSourceDbContext>, IIdentityEventSourceRepository
    {
        public IdentityEventSourceRepository(IdentityEventSourceDbContext eventSourceDbContext, IHttpContextAccessor httpContextAccessor) : base(eventSourceDbContext, httpContextAccessor)
        {

        }

        public new void AddByEvent<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            var eventSourceModel = new IdentityEventSourceModel { AggregateId = eventModel.AggregateId, EventModelName = nameof(eventModel), EventModelData = JsonConvert.SerializeObject(eventModel), Operator = _httpContextAccessor.HttpContext.User.Identity.Name ?? "" };
            _db.Set<IdentityEventSourceModel>().Add(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        }

        public new async Task AddByEventAsync<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            var eventSourceModel = new IdentityEventSourceModel { AggregateId = eventModel.AggregateId, EventModelName = nameof(eventModel), EventModelData = JsonConvert.SerializeObject(eventModel), Operator = _httpContextAccessor.HttpContext.User.Identity.Name ?? "" };
            await _db.Set<IdentityEventSourceModel>().AddAsync(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        }
    }
}
