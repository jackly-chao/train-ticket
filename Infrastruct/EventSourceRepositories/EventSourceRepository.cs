﻿using Domain.Events.Models;
using Domain.EventSources.Handlers;
using Domain.EventSources.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastruct.EventSourceRepositories
{
    /// <summary>
    /// 事件溯源仓储
    /// </summary>
    /// <typeparam name="TEventSourceDbContext">泛型事件溯源模型数据库上下文</typeparam>
    public abstract class EventSourceRepository<TEventSourceDbContext> : IEventSourceRepository where TEventSourceDbContext : DbContext
    {
        /// <summary>
        /// 事件溯源数据库上下文
        /// </summary>
        protected readonly TEventSourceDbContext _db;

        /// <summary>
        /// Http上下文
        /// </summary>
        protected readonly IHttpContextAccessor _httpContextAccessor;

        public EventSourceRepository(TEventSourceDbContext eventSourceDbContext, IHttpContextAccessor httpContextAccessor)
        {
            _db = eventSourceDbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        #region 同步方法
        public List<TEventSourceModel> GetList<TEventSourceModel>(object aggregateId) where TEventSourceModel : EventSourceModel
        {
            return _db.Set<TEventSourceModel>().Where(d => d.AggregateId.Equals(aggregateId)).ToList();
        }

        public void Add<TEventSourceModel>(TEventSourceModel eventSourceModel) where TEventSourceModel : EventSourceModel
        {
            _db.Set<TEventSourceModel>().Add(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        }

        public void AddByEvent<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            var eventSourceModel = new DefaultEventSourceModel { AggregateId = eventModel.AggregateId, EventModelName = nameof(eventModel), EventModelData = JsonConvert.SerializeObject(eventModel), Operator = _httpContextAccessor.HttpContext.User.Identity.Name ?? "" };
            _db.Set<DefaultEventSourceModel>().Add(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        }
        #endregion

        #region 异步方法
        public async Task<List<TEventSourceModel>> GetListAsync<TEventSourceModel>(object aggregateId) where TEventSourceModel : EventSourceModel
        {
            return await _db.Set<TEventSourceModel>().Where(d => d.AggregateId.Equals(aggregateId)).ToListAsync();
        }

        public async Task AddAsync<TEventSourceModel>(TEventSourceModel eventSourceModel) where TEventSourceModel : EventSourceModel
        {
            await _db.Set<TEventSourceModel>().AddAsync(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        }

        public async Task AddByEventAsync<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            var eventSourceModel = new DefaultEventSourceModel { AggregateId = eventModel.AggregateId, EventModelName = nameof(eventModel), EventModelData = JsonConvert.SerializeObject(eventModel), Operator = _httpContextAccessor.HttpContext.User.Identity.Name ?? "" };
            await _db.Set<DefaultEventSourceModel>().AddAsync(eventSourceModel);
            _db.SaveChanges();//这里才会将数据保存到数据库
        } 
        #endregion

        public void Dispose()
        {
            _db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
