﻿using CSRedis;

namespace Infrastruct.RedisClients
{
    /// <summary>
    /// 默认Redis客户端
    /// </summary>
    public class DefaultRedisClient : CSRedisClient
    {
        public DefaultRedisClient(string connectionString) : base(connectionString)
        {

        }
    }
}
