﻿using Domain.IRepositories;
using Infrastruct.DbContexts;

namespace Infrastruct.Repositories
{
    /// <summary>
    /// 认证仓储
    /// </summary>
    public class IdentityRepository : Repository<IdentityDbContext>, IIdentityRepository
    {
        public IdentityRepository(IdentityDbContext dbContext) : base(dbContext)
        {

        }
    }
}
