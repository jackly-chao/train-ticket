﻿using Domain.IRepositories;
using Domain.Models;
using Infrastruct.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Infrastruct.Repositories
{
    /// <summary>
    /// 默认仓储
    /// </summary>
    public class DefaultRepository : Repository<DefaultDbContext>, IDefaultRepository
    {
        public DefaultRepository(DefaultDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<DefaultDomainModel> GetByNameAsync(string name)
        {
            return await _db.Set<DefaultDomainModel>().FirstOrDefaultAsync(d => d.Name == name);
        }

        public async Task<DefaultDomainModel> GetByEnNameAsync(string enName)
        {
            return await _db.Set<DefaultDomainModel>().FirstOrDefaultAsync(d => d.EnName == enName);
        }
    }
}
