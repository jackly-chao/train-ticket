﻿using Domain.IRepositories;
using Domain.Models;
using Extension.Attributes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastruct.Repositories
{
    /// <summary>
    /// 仓储
    /// </summary>
    /// <typeparam name="TDbContext">泛型数据上下文</typeparam>
    public abstract class Repository<TDbContext> : IRepository where TDbContext : DbContext
    {
        /// <summary>
        /// 数据上下文
        /// </summary>
        protected readonly TDbContext _db;

        public Repository(TDbContext dbContext)
        {
            _db = dbContext;
        }

        #region 同步方法
        public List<TDomainModel> GetList<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel
        {
            return _db.Set<TDomainModel>().Where(predicate).AsNoTracking().ToList();//AsNoTracking是为了解决“无法跟踪实体类型“***”的实例，因为已经在跟踪另一个与{“ID”}具有相同键值的实例”的问题
        }

        public List<TDomainModel> GetList<TDomainModel>() where TDomainModel : DomainModel
        {
            return _db.Set<TDomainModel>().AsNoTracking().ToList();
        }

        public TDomainModel Get<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel
        {
            return _db.Set<TDomainModel>().AsNoTracking().SingleOrDefault(predicate);
        }

        [GetCacheById]
        public TDomainModel GetById<TDomainModel>(object id) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(id);
            _db.Entry(model).State = EntityState.Detached;//解决“无法跟踪实体类型“***”的实例，因为已经在跟踪另一个与{“ID”}具有相同键值的实例”的问题
            return model;
        }

        public void Add<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel
        {
            _db.Add(domainModel);
        }

        public void Update<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(domainModel.Id);
            _db.Entry(model).State = EntityState.Detached;
            domainModel.CreateTime = model.CreateTime;//这里只是为了不更新创建时间
            _db.Set<TDomainModel>().Update(domainModel);
        }

        public void DeleteById<TDomainModel>(object id) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(id);
            _db.Entry(model).State = EntityState.Detached;
            _db.Set<TDomainModel>().Remove(model);
        }

        public int Save()
        {
            return _db.SaveChanges();
        }
        #endregion

        #region 异步方法
        public async Task<List<TDomainModel>> GetListAsync<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel
        {
            return await _db.Set<TDomainModel>().Where(predicate).AsNoTracking().ToListAsync();
        }

        public async Task<List<TDomainModel>> GetListAsync<TDomainModel>() where TDomainModel : DomainModel
        {
            return await _db.Set<TDomainModel>().AsNoTracking().ToListAsync();
        }

        public async Task<TDomainModel> GetAsync<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel
        {
            return await _db.Set<TDomainModel>().AsNoTracking().SingleOrDefaultAsync(predicate);
        }

        [GetCacheById]
        public async Task<TDomainModel> GetByIdAsync<TDomainModel>(object id) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(id);
            _db.Entry(model).State = EntityState.Detached;
            return await Task.FromResult(model);
        }

        public async Task AddAsync<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel
        {
            await _db.AddAsync(domainModel);
        }

        public async Task UpdateAsync<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(domainModel.Id);
            _db.Entry(model).State = EntityState.Detached;
            domainModel.CreateTime = model.CreateTime;//这里只是为了不更新创建时间
            _db.Set<TDomainModel>().Update(domainModel);
            await Task.CompletedTask;
        }

        public async Task DeleteByIdAsync<TDomainModel>(object id) where TDomainModel : DomainModel
        {
            var model = _db.Set<TDomainModel>().Find(id);
            _db.Entry(model).State = EntityState.Detached;
            _db.Set<TDomainModel>().Remove(model);
            await Task.CompletedTask;
        }

        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }
        #endregion

        public void Dispose()
        {
            _db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
