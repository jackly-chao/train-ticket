﻿using Domain.IMongos;
using Domain.MongoModels;
using Infrastruct.MongoSettings;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastruct.Mongos
{
    /// <summary>
    /// Mongo
    /// </summary>
    /// <typeparam name="TMongoSetting">泛型Mongo配置</typeparam>
    public abstract class Mongo<TMongoSetting> : IMongo where TMongoSetting : MongoSetting
    {
        /// <summary>
        /// Mongo配置
        /// </summary>
        protected readonly TMongoSetting _mongoSetting;

        /// <summary>
        /// Mongo数据库
        /// </summary>
        protected static IMongoDatabase _db = null;

        /// <summary>
        /// 锁元
        /// </summary>
        private static readonly object _lockObj = new();

        /// <summary>
        /// 查询Mongo数据库
        /// </summary>
        /// <returns></returns>
        private IMongoDatabase GetMongoDatabase()
        {
            if (_db == null)
            {
                lock (_lockObj)//单例模式
                {
                    var serializer = new DateTimeSerializer(DateTimeKind.Local, BsonType.DateTime);
                    BsonSerializer.RegisterSerializer(typeof(DateTime), serializer);//设置mongodb时区
                    var client = new MongoClient(_mongoSetting.ConnectionString);
                    _db = client.GetDatabase(_mongoSetting.DbName);
                }
            }
            return _db;
        }

        public Mongo(TMongoSetting mongoSetting)
        {
            _mongoSetting = mongoSetting;
            _db = GetMongoDatabase();
        }

        #region 同步方法
        public List<TMongoModel> GetList<TMongoModel>(Expression<Func<TMongoModel, bool>> filter) where TMongoModel : MongoModel
        {
            return _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(filter).ToList();
        }

        public List<TMongoModel> GetList<TMongoModel>(FilterDefinition<TMongoModel> filter = null) where TMongoModel : MongoModel
        {
            return _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(filter).ToList();
        }

        public TMongoModel GetById<TMongoModel>(object id) where TMongoModel : MongoModel
        {
            return _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(d => d.Id == (ObjectId)id).FirstOrDefault();
        }

        public void Add<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel
        {
            _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).InsertOne(mongoModel);
        }

        public void Update<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel
        {
            _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).ReplaceOne(Builders<TMongoModel>.Filter.Where(d => d.Id == mongoModel.Id), mongoModel);
        }

        public void DeleteById<TMongoModel>(object id) where TMongoModel : MongoModel
        {
            _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).DeleteOne(Builders<TMongoModel>.Filter.Where(d => d.Id == (ObjectId)id));
        }
        #endregion

        #region 异步方法
        public async Task<List<TMongoModel>> GetListAsync<TMongoModel>(Expression<Func<TMongoModel, bool>> filter) where TMongoModel : MongoModel
        {
            return await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(filter).ToListAsync();
        }

        public async Task<List<TMongoModel>> GetListAsync<TMongoModel>(FilterDefinition<TMongoModel> filter = null) where TMongoModel : MongoModel
        {
            return await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(filter).ToListAsync();
        }

        public async Task<TMongoModel> GetByIdAsync<TMongoModel>(object id) where TMongoModel : MongoModel
        {
            return await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).Find(d => d.Id == (ObjectId)id).FirstOrDefaultAsync();
        }

        public async Task AddAsync<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel
        {
            await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).InsertOneAsync(mongoModel);
        }

        public async Task UpdateAsync<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel
        {
            await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).ReplaceOneAsync(Builders<TMongoModel>.Filter.Where(d => d.Id == mongoModel.Id), mongoModel);
        }

        public async Task DeleteByIdAsync<TMongoModel>(object id) where TMongoModel : MongoModel
        {
            await _db.GetCollection<TMongoModel>(typeof(TMongoModel).Name).DeleteOneAsync(Builders<TMongoModel>.Filter.Where(d => d.Id == (ObjectId)id));
        }
        #endregion
    }
}
