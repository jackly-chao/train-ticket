﻿using Domain.IMongos;
using Infrastruct.MongoSettings;

namespace Infrastruct.Mongos
{
    /// <summary>
    /// 默认Mongo
    /// </summary>
    public class DefaultMongo : Mongo<DefaultMongoSetting>, IDefaultMongo
    {
        public DefaultMongo(DefaultMongoSetting mongoSetting) : base(mongoSetting)
        {

        }
    }
}
