﻿using Domain.Models;
using Infrastruct.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastruct.DbContexts
{
    /// <summary>
    /// 默认数据库上下文
    /// </summary>
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {

        }
    
        /// <summary>
        /// 默认领域模型集合
        /// </summary>
        public DbSet<DefaultDomainModel> Defaults { get; set; }

        /// <summary>
        /// 菜单领域模型集合
        /// </summary>
        public DbSet<MenuDomainModel> Menus { get; set; }

        /// <summary>
        /// 权限领域模型集合
        /// </summary>
        public DbSet<PermissionDomainModel> Permissions { get; set; }

        /// <summary>
        /// 角色权限领域模型集合
        /// </summary>
        public DbSet<RolePermissionDomainModel> RolePermissions { get; set; }

        /// <summary>
        /// 猫咪领域模型集合
        /// </summary>
        public DbSet<CatDomainModel> Cats { get; set; }

        /// <summary>
        /// 配置领域模型->数据模型(数据表)映射
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DefaultEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MenuEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CatEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }

    /// <summary>
    /// 生成迁移文件必备，参考：https://docs.microsoft.com/zh-cn/ef/core/cli/dbcontext-creation?tabs=dotnet-core-cli
    /// </summary>
    public class DefaultDbContextFactory : IDesignTimeDbContextFactory<DefaultDbContext>
    {
        public DefaultDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DefaultDbContext>();
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=TestDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;");

            return new DefaultDbContext(optionsBuilder.Options);
        }
    }
}
