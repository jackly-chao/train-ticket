﻿using Domain.Models;
using Infrastruct.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Infrastruct.DbContexts
{
    /// <summary>
    /// 认证数据库上下文
    /// </summary>
    public class IdentityDbContext : DbContext
    {
        public IdentityDbContext(DbContextOptions<IdentityDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// 用户领域模型集合
        /// </summary>
        public DbSet<UserDomainModel> Users { get; set; }

        /// <summary>
        /// 角色领域模型集合
        /// </summary>
        public DbSet<RoleDomainModel> Roles { get; set; }

        /// <summary>
        /// 用户角色领域模型集合
        /// </summary>
        public DbSet<UserRoleDomainModel> UserRoles { get; set; }

        /// <summary>
        /// 用户声明领域模型集合
        /// </summary>
        public DbSet<UserClaimDomainModel> UserClaims { get; set; }

        /// <summary>
        /// 配置领域模型->数据模型(数据表)映射
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserClaimEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
