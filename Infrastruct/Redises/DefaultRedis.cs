﻿using Domain.IRedises;
using Infrastruct.RedisClients;

namespace Infrastruct.Redises
{
    /// <summary>
    /// 默认Redis
    /// </summary>
    public class DefaultRedis : Redis<DefaultRedisClient>, IDefaultRedis
    {
        public DefaultRedis(DefaultRedisClient redisClient) : base(redisClient)
        {

        }
    }
}
