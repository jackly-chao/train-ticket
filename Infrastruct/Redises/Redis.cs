﻿using CSRedis;
using Domain.IRedises;
using System.Threading.Tasks;

namespace Infrastruct.Redises
{
    /// <summary>
    /// Redis
    /// </summary>
    /// <typeparam name="TRedisClient">泛型Redis客户端</typeparam>
    public abstract class Redis<TRedisClient> : IRedis where TRedisClient : CSRedisClient
    {
        /// <summary>
        /// Redis客户端
        /// </summary>
        protected readonly TRedisClient _redisClient;

        public Redis(TRedisClient redisClient)
        {
            _redisClient = redisClient;
        }

        #region 同步方法
        public T Get<T>(string key)
        {
            return _redisClient.Get<T>(key);
        }

        public bool Set(string key, object value, int expireSeconds = -1)
        {
            return _redisClient.Set(key, value, expireSeconds);
        }

        public long Del(params string[] key)
        {
            return _redisClient.Del(key);
        }
        #endregion

        #region 异步方法
        public async Task<T> GetAsync<T>(string key)
        {
            return await _redisClient.GetAsync<T>(key);
        }

        public async Task<bool> SetAsync(string key, object value, int expireSeconds = -1)
        {
            return await _redisClient.SetAsync(key, value, expireSeconds);
        }

        public async Task<long> DelAsync(params string[] key)
        {
            return await _redisClient.DelAsync(key);
        }
        #endregion
    }
}
