﻿using Domain.EventSources.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastruct.EventSourceEntityTypeConfigurations
{
    /// <summary>
    /// 认证事件溯源模型->数据模型(数据表)映射
    /// </summary>
    public class IdentityEventSourceEntityTypeConfiguration : IEntityTypeConfiguration<IdentityEventSourceModel>
    {
        public void Configure(EntityTypeBuilder<IdentityEventSourceModel> builder)
        {
            builder.ToTable("IdentityEventSource");
        }
    }
}
