﻿using Domain.EventSources.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastruct.EventSourceEntityTypeConfigurations
{
    /// <summary>
    /// 默认事件溯源模型->数据模型(数据表)映射
    /// </summary>
    public class DefaultEventSourceEntityTypeConfiguration : IEntityTypeConfiguration<DefaultEventSourceModel>
    {
        public void Configure(EntityTypeBuilder<DefaultEventSourceModel> builder)
        {
            builder.ToTable("DefaultEventSource");
        }
    }
}
