﻿using Domain.EventSources.Models;
using Infrastruct.EventSourceEntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastruct.EventSourceDbContexts
{
    /// <summary>
    /// 认证事件溯源数据库上下文
    /// </summary>
    public class IdentityEventSourceDbContext : DbContext
    {
        public IdentityEventSourceDbContext(DbContextOptions<IdentityEventSourceDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// 认证事件溯源模型集合
        /// </summary>
        public DbSet<IdentityEventSourceModel> IdentityEventSources { get; set; }

        /// <summary>
        /// 配置事件溯源模型和数据模型的映射
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new IdentityEventSourceEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }

    /// <summary>
    /// 生成迁移文件必备，参考：https://docs.microsoft.com/zh-cn/ef/core/cli/dbcontext-creation?tabs=dotnet-core-cli
    /// </summary>
    public class IdentityEventSourceDbContextFactory : IDesignTimeDbContextFactory<IdentityEventSourceDbContext>
    {
        public IdentityEventSourceDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IdentityEventSourceDbContext>();
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=TestDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;");

            return new IdentityEventSourceDbContext(optionsBuilder.Options);
        }
    }
}
