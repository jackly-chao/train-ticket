﻿using Domain.EventSources.Models;
using Infrastruct.EventSourceEntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastruct.EventSourceDbContexts
{
    /// <summary>
    /// 默认事件溯源数据库上下文
    /// </summary>
    public class DefaultEventSourceDbContext : DbContext
    {
        public DefaultEventSourceDbContext(DbContextOptions<DefaultEventSourceDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// 默认事件溯源模型集合
        /// </summary>
        public DbSet<DefaultEventSourceModel> DefaultEventSources { get; set; }

        /// <summary>
        /// 配置事件溯源模型和数据模型的映射
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DefaultEventSourceEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }

    /// <summary>
    /// 生成迁移文件必备，参考：https://docs.microsoft.com/zh-cn/ef/core/cli/dbcontext-creation?tabs=dotnet-core-cli
    /// </summary>
    public class DefaultEventSourceDbContextFactory : IDesignTimeDbContextFactory<DefaultEventSourceDbContext>
    {
        public DefaultEventSourceDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DefaultEventSourceDbContext>();
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=TestDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;");

            return new DefaultEventSourceDbContext(optionsBuilder.Options);
        }
    }
}
