﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastruct.EntityTypeConfigurations
{
    /// <summary>
    /// 用户领域模型->数据模型(数据表)映射
    /// </summary>
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<UserDomainModel>
    {
        public void Configure(EntityTypeBuilder<UserDomainModel> builder)
        {
            builder.ToTable("AspNetUsers");
        }
    }

    /// <summary>
    /// 角色领域模型->数据模型(数据表)映射
    /// </summary>
    public class RoleEntityTypeConfiguration : IEntityTypeConfiguration<RoleDomainModel>
    {
        public void Configure(EntityTypeBuilder<RoleDomainModel> builder)
        {
            builder.ToTable("AspNetRoles");
        }
    }

    /// <summary>
    /// 用户角色领域模型->数据模型(数据表)映射
    /// </summary>
    public class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<UserRoleDomainModel>
    {
        public void Configure(EntityTypeBuilder<UserRoleDomainModel> builder)
        {
            builder.ToTable("AspNetUserRoles");
            builder.Ignore(d => d.Id).Ignore(d => d.Status).Ignore(d => d.CreateTime).Ignore(d => d.UpdateTime);
            builder.HasKey(d => new { d.UserId, d.RoleId });
        }
    }

    /// <summary>
    /// 用户声明领域模型->数据模型(数据表)映射
    /// </summary>
    public class UserClaimEntityTypeConfiguration : IEntityTypeConfiguration<UserClaimDomainModel>
    {
        public void Configure(EntityTypeBuilder<UserClaimDomainModel> builder)
        {
            builder.ToTable("AspNetUserClaims");
            builder.Ignore(d => d.Status).Ignore(d => d.CreateTime).Ignore(d => d.UpdateTime);
        }
    }
}
