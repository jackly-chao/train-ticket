﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastruct.EntityTypeConfigurations
{
    /// <summary>
    /// 默认领域模型->数据模型(数据表)映射
    /// </summary>
    public class DefaultEntityTypeConfiguration : IEntityTypeConfiguration<DefaultDomainModel>
    {
        public void Configure(EntityTypeBuilder<DefaultDomainModel> builder)
        {
            builder.ToTable("Default");

            builder.Property(d => d.Id)
                 .HasColumnName("Id");
            builder.Property(d => d.Name)
                .HasColumnName("Name");
            builder.Property(d => d.EnName)
                .HasColumnName("EnName");
            builder.Property(d => d.Status)
              .HasColumnName("Status");
            builder.Property(d => d.CreateTime)
              .HasColumnName("CreateTime");
            builder.Property(d => d.UpdateTime)
              .HasColumnName("UpdateTime");
        }
    }

    /// <summary>
    /// 菜单领域模型->数据模型(数据表)映射
    /// </summary>
    public class MenuEntityTypeConfiguration : IEntityTypeConfiguration<MenuDomainModel>
    {
        public void Configure(EntityTypeBuilder<MenuDomainModel> builder)
        {
            builder.ToTable("Menu");
        }
    }

    /// <summary>
    /// 权限领域模型->数据模型(数据表)映射
    /// </summary>
    public class PermissionEntityTypeConfiguration : IEntityTypeConfiguration<PermissionDomainModel>
    {
        public void Configure(EntityTypeBuilder<PermissionDomainModel> builder)
        {
            builder.ToTable("Permission");
        }
    }

    /// <summary>
    /// 角色权限领域模型->数据模型(数据表)映射
    /// </summary>
    public class RolePermissionEntityTypeConfiguration : IEntityTypeConfiguration<RolePermissionDomainModel>
    {
        public void Configure(EntityTypeBuilder<RolePermissionDomainModel> builder)
        {
            builder.ToTable("RolePermission");
        }
    }

    /// <summary>
    /// 猫咪领域模型->数据模型(数据表)映射
    /// </summary>
    public class CatEntityTypeConfiguration : IEntityTypeConfiguration<CatDomainModel>
    {
        public void Configure(EntityTypeBuilder<CatDomainModel> builder)
        {
            builder.ToTable("Cat");
        }
    }
}
