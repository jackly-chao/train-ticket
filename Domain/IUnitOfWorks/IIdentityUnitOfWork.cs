﻿namespace Domain.IUnitOfWorks
{
    /// <summary>
    /// 认证工作单元接口
    /// </summary>
    public interface IIdentityUnitOfWork : IUnitOfWork
    {

    }
}
