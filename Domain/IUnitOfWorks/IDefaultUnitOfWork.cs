﻿namespace Domain.IUnitOfWorks
{
    /// <summary>
    /// 默认工作单元接口
    /// </summary>
    public interface IDefaultUnitOfWork : IUnitOfWork
    {

    }
}
