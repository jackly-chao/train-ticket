﻿using System;
using System.Threading.Tasks;

namespace Domain.IUnitOfWorks
{
    /// <summary>
    /// 工作单元接口，这里提交以后才会将数据库新增、修改和删除后的数据保存到数据库，并且工作单元可以处理跨库/表事务问题
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        #region 同步方法
        /// <summary>
        /// 提交
        /// </summary>
        /// <returns></returns>
        int Commit();

        /// <summary>
        /// 通过事务提交
        /// </summary>
        /// <returns></returns>
        int CommitByTransaction();
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步提交
        /// </summary>
        /// <returns></returns>
        Task<int> CommitAsync();

        /// <summary>
        /// 通过事务异步提交
        /// </summary>
        /// <returns></returns>
        Task<int> CommitByTransactionAsync();
        #endregion
    }
}
