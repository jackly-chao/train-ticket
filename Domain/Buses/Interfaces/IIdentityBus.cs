﻿namespace Domain.Buses.Interfaces
{
    /// <summary>
    /// 认证总线(中介者)处理接口
    /// </summary>
    public interface IIdentityBus : IBus
    {

    }
}
