﻿namespace Domain.Buses.Interfaces
{
    /// <summary>
    /// 默认总线(中介者)处理接口
    /// </summary>
    public interface IDefaultBus : IBus
    {

    }
}
