﻿using Domain.Buses.Interfaces;
using Domain.EventSources.Handlers;
using MediatR;

namespace Domain.Buses.Handlers
{
    /// <summary>
    /// 认证总线(中介者)处理
    /// </summary>
    public class IdentityBus : Bus<IIdentityEventSourceRepository>, IIdentityBus
    {
        public IdentityBus(IMediator mediator, IIdentityEventSourceRepository eventSourceRepository) : base(mediator, eventSourceRepository)
        {

        }
    }
}
