﻿using Domain.Buses.Interfaces;
using Domain.EventSources.Handlers;
using MediatR;

namespace Domain.Buses.Handlers
{
    /// <summary>
    /// 默认总线(中介者)处理
    /// </summary>
    public class DefaultBus : Bus<IDefaultEventSourceRepository>, IDefaultBus
    {
        public DefaultBus(IMediator mediator, IDefaultEventSourceRepository eventSourceRepository) : base(mediator, eventSourceRepository)
        {

        }
    }
}
