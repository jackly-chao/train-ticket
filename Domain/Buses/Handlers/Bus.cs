﻿using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.Events.Models;
using Domain.EventSources.Handlers;
using Domain.Notifications.Models;
using MediatR;
using System.Threading.Tasks;

namespace Domain.Buses.Handlers
{
    /// <summary>
    /// 总线(中介者)处理
    /// </summary>
    /// <typeparam name="TIEventSourceRepository">泛型事件溯源模型仓储接口</typeparam>
    public abstract class Bus<TIEventSourceRepository> : IBus where TIEventSourceRepository : IEventSourceRepository
    {
        /// <summary>
        /// 中介者
        /// </summary>
        protected readonly IMediator _mediator;

        /// <summary>
        /// 事件溯源模型仓储接口
        /// </summary>
        protected readonly TIEventSourceRepository _eventSourceRepository;

        public Bus(IMediator mediator, TIEventSourceRepository eventSourceRepository)
        {
            _mediator = mediator;
            _eventSourceRepository = eventSourceRepository;
        }

        public async Task SendCommandAsync<TCommandModel>(TCommandModel commandModel) where TCommandModel : CommandModel
        {
            await _mediator.Send(commandModel);
        }

        public async Task RaiseEventAsync<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            await _eventSourceRepository.AddByEventAsync(eventModel);
            await _mediator.Publish(eventModel);
        }

        public async Task RaiseNotificationAsync<TNotificationModel>(TNotificationModel notificationModel) where TNotificationModel : NotificationModel
        {
            await _mediator.Publish(notificationModel);
        }
    }
}
