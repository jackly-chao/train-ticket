﻿using Domain.IRedises;
using Domain.Notifications.Models;
using Microsoft.AspNetCore.Http;

namespace Domain.Notifications.Handlers
{
    /// <summary>
    /// 默认通知处理
    /// </summary>
    public class DefaultNotificationHandler : NotificationHandler<DefaultNotificationModel, IDefaultRedis>
    {
        public DefaultNotificationHandler(IDefaultRedis redis, IHttpContextAccessor httpContext) : base(redis, httpContext)
        {

        }
    }
}
