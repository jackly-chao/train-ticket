﻿using Domain.IRedises;
using Domain.Notifications.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Notifications.Handlers
{
    /// <summary>
    /// 通知处理
    /// </summary>
    public abstract class NotificationHandler<TNotificationModel, TIRedis> : INotificationHandler<TNotificationModel> where TNotificationModel : NotificationModel where TIRedis : IRedis
    {
        protected TIRedis _redis;

        private readonly IHttpContextAccessor _httpContext;

        private string _cacheKey;

        public NotificationHandler(TIRedis redis, IHttpContextAccessor httpContext)
        {
            _redis = redis;
            _httpContext = httpContext;
            _cacheKey = "Notifications:" + (_httpContext.HttpContext.User.Identity.Name ?? "Defalt");
        }

        /// <summary>
        /// 处理方法
        /// </summary>
        /// <param name="notification">通知模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(TNotificationModel notification, CancellationToken cancellationToken)
        {
            var notificationModels = _redis.Get<List<TNotificationModel>>(_cacheKey) ?? new List<TNotificationModel>();
            if (!notificationModels.Any(d => d.Id == notification.Id)) notificationModels.Add(notification);//会监听两次，不知道问题点在哪，先这么处理吧
            _redis.Set(_cacheKey, notificationModels);
            await Task.CompletedTask;
        }

        /// <summary>
        /// 查询当前生命周期内的通知列表
        /// </summary>
        /// <returns></returns>
        public virtual List<TNotificationModel> GetList(bool clearNotificationAfterGet = true)
        {
            var notificationModels = _redis.Get<List<TNotificationModel>>(_cacheKey) ?? new List<TNotificationModel>();
            if (clearNotificationAfterGet) Dispose();
            return notificationModels;
        }

        /// <summary>
        /// 判断当前生命周期内是否存在通知
        /// </summary>
        /// <returns></returns>
        public virtual bool HasNotifications()
        {
            return GetList(false).Any();
        }

        /// <summary>
        /// 回收(清空通知)
        /// </summary>
        public void Dispose()
        {
            _redis.Del(_cacheKey);
        }
    }
}
