﻿using MediatR;
using System;

namespace Domain.Notifications.Models
{
    /// <summary>
    /// 通知模型
    /// </summary>
    public abstract class NotificationModel : INotification
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }
}
