﻿using Common.Enums;
using System;

namespace Domain.Models
{
    /// <summary>
    /// 用户领域模型
    /// </summary>
    public class UserDomainModel : DomainModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 哈希后的密码
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 姓氏
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 名
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; } = (int)SexEnum.Male;

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birth { get; set; } = DateTime.Now;

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 查询全名
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return $"{LastName}/{FirstName}";
        }
    }
}
