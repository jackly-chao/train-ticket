﻿namespace Domain.Models
{
    /// <summary>
    /// 默认领域模型
    /// </summary>
    public class DefaultDomainModel : DomainModel
    {
        public DefaultDomainModel(int id, string name, string enName)
        {
            Id = id;
            Name = name;
            EnName = enName;
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }
    }
}
