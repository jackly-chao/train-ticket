﻿namespace Domain.Models
{
    /// <summary>
    /// 角色权限领域模型
    /// </summary>
    public class RolePermissionDomainModel : DomainModel
    {
        public RolePermissionDomainModel(int id, int roleId, int permissionId, int status)
        {
            Id = id;
            RoleId = roleId;
            PermissionId = permissionId;
            Status = status;
        }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; set; }
    }
}
