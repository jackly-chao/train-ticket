﻿namespace Domain.Models
{
    /// <summary>
    /// 菜单领域模型
    /// </summary>
    public class MenuDomainModel : DomainModel
    {
        public MenuDomainModel(int id, string name, string enName,int status)
        {
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }
    }
}
