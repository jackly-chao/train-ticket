﻿namespace Domain.Models
{
    /// <summary>
    /// 用户声明领域模型
    /// </summary>
    public class UserClaimDomainModel : DomainModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 声明类型
        /// </summary>
        public string ClaimType { get; set; }

        /// <summary>
        /// 声明值
        /// </summary>
        public string ClaimValue { get; set; }
    }
}
