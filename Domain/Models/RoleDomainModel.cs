﻿namespace Domain.Models
{
    /// <summary>
    /// 角色领域模型
    /// </summary>
    public class RoleDomainModel : DomainModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }
    }
}
