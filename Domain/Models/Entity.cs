﻿using Common.Enums;
using System;

namespace Domain.Models
{
    /// <summary>
    /// 实体
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; } = (int)StatusEnum.Close;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get;  set; } = DateTime.Now;

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; protected set; } = DateTime.Now;

        /// <summary>
        /// 实体相等比较
        /// </summary>
        /// <param name="a">实体A</param>
        /// <param name="b">实体B</param>
        /// <returns></returns>
        public static bool operator ==(Entity a, Entity b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.Equals(b);
        }

        /// <summary>
        /// 实体不等比较
        /// </summary>
        /// <param name="a">实体A</param>
        /// <param name="b">实体B</param>
        /// <returns></returns>
        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            var compareObj = obj as Entity;
            if (ReferenceEquals(this, compareObj)) return true;
            return Id.Equals(compareObj.Id);
        }

        public override int GetHashCode()
        {
            return (GetType().GetHashCode() * 907) + Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"{GetType().Name} [Id={Id}]";
        }

        public bool IsOpen()
        {
            return Status == (int)StatusEnum.Open;
        }
    }
}
