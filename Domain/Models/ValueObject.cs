﻿namespace Domain.Models
{
    /// <summary>
    /// 值对象
    /// </summary>
    /// <typeparam name="T">泛型对象</typeparam>
    public abstract class ValueObject<T> where T : ValueObject<T>
    {
        /// <summary>
        /// 值对象相等比较
        /// </summary>
        /// <param name="a">值对象A</param>
        /// <param name="b">值对象B</param>
        /// <returns></returns>
        public static bool operator ==(ValueObject<T> a, ValueObject<T> b)
        {
            if (a is null && b is null) return true;
            if (a is null || b is null) return false;
            return a.Equals(b);
        }

        /// <summary>
        /// 值对象不等比较
        /// </summary>
        /// <param name="a">值对象A</param>
        /// <param name="b">值对象B</param>
        /// <returns></returns>
        public static bool operator !=(ValueObject<T> a, ValueObject<T> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            var compareObj = obj as T;
            return Equals(compareObj);
        }

        public override int GetHashCode()
        {
            return GetHashCode();
        }
    }
}
