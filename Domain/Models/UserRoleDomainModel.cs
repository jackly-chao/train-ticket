﻿namespace Domain.Models
{
    /// <summary>
    /// 用户角色领域模型
    /// </summary>
    public class UserRoleDomainModel : DomainModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
    }
}
