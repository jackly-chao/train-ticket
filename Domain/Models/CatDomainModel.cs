﻿namespace Domain.Models
{
    /// <summary>
    /// 猫咪领域模型
    /// </summary>
    public class CatDomainModel : DomainModel
    {
        public CatDomainModel(int id, string name, string enName, int status)
        {
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; private set; }
    }
}
