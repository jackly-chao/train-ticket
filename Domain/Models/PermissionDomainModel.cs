﻿namespace Domain.Models
{
    /// <summary>
    /// 权限领域模型
    /// </summary>
    public class PermissionDomainModel : DomainModel
    {
        public PermissionDomainModel(int id, string name, string enName, string route, int method, int menuId, int status)
        {
            Id = id;
            Name = name;
            EnName = enName;
            Route = route;
            Method = method;
            MenuId = menuId;
            Status = status;
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }

        /// <summary>
        /// 路由
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// 请求方法
        /// </summary>
        public int Method { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuId { get; set; }
    }
}
