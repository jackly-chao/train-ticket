﻿using System.Threading.Tasks;

namespace Domain.IRedises
{
    /// <summary>
    /// Redis接口
    /// </summary>
    public interface IRedis
    {
        #region 同步方法
        /// <summary>
        /// 查询
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        T Get<T>(string key);

        /// <summary>
        /// 设置
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存对象</param>
        /// <param name="expireSeconds">过期时间，默认为-1，即永不过期</param>
        /// <returns></returns>
        bool Set(string key, object value, int expireSeconds = -1);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        long Del(params string[] key);
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步查询
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key);

        /// <summary>
        /// 异步设置
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存对象</param>
        /// <param name="expireSeconds">过期时间，默认为-1，即永不过期</param>
        /// <returns></returns>
        Task<bool> SetAsync(string key, object value, int expireSeconds = -1);

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        Task<long> DelAsync(params string[] key);
        #endregion
    }
}
