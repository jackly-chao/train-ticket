﻿namespace Domain.IRedises
{
    /// <summary>
    /// 默认Redis接口
    /// </summary>
    public interface IDefaultRedis : IRedis
    {

    }
}
