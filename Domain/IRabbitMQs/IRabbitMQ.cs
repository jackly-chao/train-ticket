﻿using System;
using System.Threading.Tasks;

namespace Domain.IRabbitMQs
{
    /// <summary>
    /// RabbitMQ接口
    /// </summary>
    public interface IRabbitMQ : IDisposable
    {
        #region 同步方法
        /// <summary>
        /// 发布消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="queueName">队列名</param>
        /// <param name="exchangeName">交换机名</param>
        /// <param name="routingKey">路由Key，用于交换机和队列（或交换机和交换机）之间的绑定</param>
        /// <param name="durable">是否持久化，true：持久化，false：非持久化</param>
        void SendMessage<T>(T message, string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true);

        /// <summary>
        /// 消费消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="queueName">队列名</param>
        /// <param name="exchangeName">交换机名</param>
        /// <param name="routingKey">路由Key，用于交换机和队列（或交换机和交换机）之间的绑定</param>
        /// <param name="durable">是否持久化，true：持久化，false：非持久化</param>
        /// <param name="autoAck">是否自动确认收到消息，true：是，false：否</param>
        /// <returns></returns>
        T ConsumeMessage<T>(string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true, bool autoAck = false);

        /// <summary>
        /// 查询消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="queueName">队列名</param>
        /// <param name="autoAck">是否自动确认收到消息，true：是，false：否</param>
        /// <returns></returns>
        T GetMessage<T>(string queueName = null, bool autoAck = false);
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步发布消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="queueName">队列名</param>
        /// <param name="exchangeName">交换机名</param>
        /// <param name="routingKey">路由Key，用于交换机和队列（或交换机和交换机）之间的绑定</param>
        /// <param name="durable">是否持久化，true：持久化，false：非持久化</param>
        /// <returns></returns>
        Task SendMessageAsync<T>(T message, string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true);

        /// <summary>
        /// 异步消费消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="queueName">队列名</param>
        /// <param name="exchangeName">交换机名</param>
        /// <param name="routingKey">路由Key，用于交换机和队列（或交换机和交换机）之间的绑定</param>
        /// <param name="durable">是否持久化，true：持久化，false：非持久化</param>
        /// <param name="autoAck">是否自动确认收到消息，true：是，false：否</param>
        /// <returns></returns>
        Task<T> ConsumeMessageAsync<T>(string queueName = null, string exchangeName = null, string routingKey = null, bool durable = true, bool autoAck = false);

        /// <summary>
        /// 异步查询消息
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="queueName">队列名</param>
        /// <param name="autoAck">是否自动确认收到消息，true：是，false：否</param>
        /// <returns></returns>
        Task<T> GetMessageAsync<T>(string queueName = null, bool autoAck = false);
        #endregion
    }
}
