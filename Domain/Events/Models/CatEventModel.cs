﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 猫咪事件模型
    /// </summary>
    public abstract class CatEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 猫咪新增事件模型
    /// </summary>
    public  class CatAddEventModel : CatEventModel
    {
        public CatAddEventModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }
    }

    /// <summary>
    /// 猫咪修改事件模型
    /// </summary>
    public class CatUpdateEventModel : CatEventModel
    {
        public CatUpdateEventModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }
    }

    /// <summary>
    /// 猫咪删除事件模型
    /// </summary>
    public class CatDeleteEventModel : CatEventModel
    {
        public CatDeleteEventModel(int id)
        {
            AggregateId = id;
            Id = id;
        }
    }
}
