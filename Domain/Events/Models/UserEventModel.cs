﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 用户事件模型
    /// </summary>
    public abstract class UserEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; protected set; }
    }

    /// <summary>
    /// 用户登录事件模型
    /// </summary>
    public class UserLoginEventModel : UserEventModel
    {
        public UserLoginEventModel(int id, string userName)
        {
            AggregateId = id;
            Id = id;
            UserName = userName;
        }
    }
}
