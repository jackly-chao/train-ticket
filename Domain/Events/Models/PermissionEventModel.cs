﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 权限事件模型
    /// </summary>
    public abstract class PermissionEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 路由
        /// </summary>
        public string Route { get; protected set; }

        /// <summary>
        /// 请求方法
        /// </summary>
        public int Method { get; protected set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuId { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 权限新增事件模型
    /// </summary>
    public class PermissionAddEventModel : PermissionEventModel
    {
        public PermissionAddEventModel(int id, string name, string enName, string route, int method, int menuId, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Route = route;
            Method = method;
            MenuId = menuId;
            Status = status;
        }
    }

    /// <summary>
    /// 权限修改事件模型
    /// </summary>
    public class PermissionUpdateEventModel : PermissionEventModel
    {
        public PermissionUpdateEventModel(int id, string name, string enName, string route, int method, int menuId, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Route = route;
            Method = method;
            MenuId = menuId;
            Status = status;
        }
    }

    /// <summary>
    /// 权限删除事件模型
    /// </summary>
    public class PermissionDeleteEventModel : PermissionEventModel
    {
        public PermissionDeleteEventModel(int id)
        {
            AggregateId = id;
            Id = id;
        }
    }
}
