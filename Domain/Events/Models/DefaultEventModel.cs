﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 默认事件模型
    /// </summary>
    public abstract class DefaultEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }
    }

    /// <summary>
    /// 默认新增事件模型
    /// </summary>
    public class DefaultAddEventModel : DefaultEventModel
    {
        public DefaultAddEventModel(int id, string name, string enName)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
        }
    }

    /// <summary>
    /// 默认修改事件模型
    /// </summary>
    public class DefaultUpdateEventModel : DefaultEventModel
    {
        public DefaultUpdateEventModel(int id, string name, string enName)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
        }
    }

    /// <summary>
    /// 默认删除事件模型
    /// </summary>
    public class DefaultDeleteEventModel : DefaultEventModel
    {
        public DefaultDeleteEventModel(int id)
        {
            AggregateId = id;
            Id = id;
        }
    }
}
