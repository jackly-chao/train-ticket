﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 角色权限事件模型
    /// </summary>
    public abstract class RolePermissionEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; protected set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 角色权限新增事件模型
    /// </summary>
    public class RolePermissionAddEventModel : RolePermissionEventModel
    {
        public RolePermissionAddEventModel(int id, int roleId, int permissionId, int status)
        {
            AggregateId = id;
            Id = id;
            RoleId = roleId;
            PermissionId = permissionId;
            Status = status;
        }
    }

    /// <summary>
    /// 角色权限修改事件模型
    /// </summary>
    public class RolePermissionUpdateEventModel : RolePermissionEventModel
    {
        public RolePermissionUpdateEventModel(int id, int roleId, int permissionId, int status)
        {
            AggregateId = id;
            Id = id;
            RoleId = roleId;
            PermissionId = permissionId;
            Status = status;
        }
    }

    /// <summary>
    /// 角色权限删除事件模型
    /// </summary>
    public class RolePermissionDeleteEventModel : RolePermissionEventModel
    {
        public RolePermissionDeleteEventModel(int id)
        {
            AggregateId = id;
            Id = id;
        }
    }
}
