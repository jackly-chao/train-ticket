﻿namespace Domain.Events.Models
{
    /// <summary>
    /// 菜单事件模型
    /// </summary>
    public abstract class MenuEventModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 菜单新增事件模型
    /// </summary>
    public class MenuAddEventModel : MenuEventModel
    {
        public MenuAddEventModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }
    }

    /// <summary>
    /// 菜单修改事件模型
    /// </summary>
    public class MenuUpdateEventModel : MenuEventModel
    {
        public MenuUpdateEventModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }
    }

    /// <summary>
    /// 菜单删除事件模型
    /// </summary>
    public class MenuDeleteEventModel : MenuEventModel
    {
        public MenuDeleteEventModel(int id)
        {
            AggregateId = id;
            Id = id;
        }
    }
}
