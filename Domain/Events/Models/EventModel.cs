﻿using MediatR;
using System;

namespace Domain.Events.Models
{
    /// <summary>
    /// 事件模型
    /// </summary>
    public abstract class EventModel : INotification
    {
        /// <summary>
        /// 聚合根ID
        /// </summary>
        public int AggregateId { get; set; }

        /// <summary>
        /// 事件模型名
        /// </summary>
        public string EventModelName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
