﻿using Domain.Events.Models;
using Domain.IMongos;
using Domain.IRabbitMQs;
using Domain.IRedises;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Events.Handlers
{
    /// <summary>
    /// 默认事件处理
    /// </summary>
    public class DefaultEventHandler : EventHandler<IDefaultRedis, IDefaultMongo, IDefaultRabbitMQ>, INotificationHandler<DefaultAddEventModel>, INotificationHandler<DefaultUpdateEventModel>, INotificationHandler<DefaultDeleteEventModel>
    {
        public DefaultEventHandler(IDefaultRedis redis, IDefaultMongo mongo, IDefaultRabbitMQ rabbitMQ) : base(redis, mongo, rabbitMQ)
        {

        }

        /// <summary>
        /// 默认新增事件处理
        /// </summary>
        /// <param name="eventModel">新增事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(DefaultAddEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleAddEventAsync(eventModel);
        }

        /// <summary>
        /// 默认修改事件处理
        /// </summary>
        /// <param name="eventModel">修改事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(DefaultUpdateEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleUpdateEventAsync(eventModel);
        }

        /// <summary>
        /// 默认删除事件处理
        /// </summary>
        /// <param name="eventModel">删除事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(DefaultDeleteEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleDeleteEventAsync(eventModel);
        }
    }
}
