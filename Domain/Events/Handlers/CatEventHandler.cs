﻿using Domain.Events.Models;
using Domain.IMongos;
using Domain.IRabbitMQs;
using Domain.IRedises;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Events.Handlers
{
    /// <summary>
    /// 猫咪事件处理
    /// </summary>
    public class CatEventHandler : EventHandler<IDefaultRedis, IDefaultMongo, IDefaultRabbitMQ>, INotificationHandler<CatAddEventModel>, INotificationHandler<CatUpdateEventModel>, INotificationHandler<CatDeleteEventModel>
    {
        public CatEventHandler(IDefaultRedis redis, IDefaultMongo mongo, IDefaultRabbitMQ rabbitMQ) : base(redis, mongo, rabbitMQ)
        {

        }

        /// <summary>
        ///  新增事件处理
        /// </summary>
        /// <param name="eventModel">新增事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(CatAddEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleAddEventAsync(eventModel);
        }

        /// <summary>
        ///  修改事件处理
        /// </summary>
        /// <param name="eventModel">修改事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(CatUpdateEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleUpdateEventAsync(eventModel);
        }

        /// <summary>
        /// 删除事件处理
        /// </summary>
        /// <param name="eventModel">删除事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(CatDeleteEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleDeleteEventAsync(eventModel);
        }
    }
}
