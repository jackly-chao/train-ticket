﻿using Domain.Events.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Events.Handlers
{
    /// <summary>
    /// 用户事件处理
    /// </summary>
    public class UserEventHandler : INotificationHandler<UserLoginEventModel>
    {
        /// <summary>
        /// 用户登录事件处理
        /// </summary>
        /// <param name="notification">用户登录事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(UserLoginEventModel notification, CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
        }
    }
}
