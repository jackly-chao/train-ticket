﻿using Domain.Events.Models;
using Domain.IMongos;
using Domain.IRabbitMQs;
using Domain.IRedises;
using Domain.MongoModels;
using System.Threading.Tasks;

namespace Domain.Events.Handlers
{
    /// <summary>
    /// 事件处理
    /// </summary>
    public abstract class EventHandler<TIRedis, TIMongo, TIRabbitMQ> where TIRedis : IRedis where TIMongo : IMongo where TIRabbitMQ : IRabbitMQ
    {
        /// <summary>
        /// Redis
        /// </summary>
        protected readonly TIRedis _redis;

        /// <summary>
        /// Mongo
        /// </summary>
        protected readonly TIMongo _mongo;

        /// <summary>
        /// RabbitMQ
        /// </summary>
        protected readonly TIRabbitMQ _rabbitMQ;

        public EventHandler(TIRedis redis, TIMongo mongo, TIRabbitMQ rabbitMQ)
        {
            _redis = redis;
            _mongo = mongo;
            _rabbitMQ = rabbitMQ;
        }

        /// <summary>
        /// 新增事件处理
        /// </summary>
        /// <typeparam name="TAddEventModel">泛型新增事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        /// <returns></returns>
        public virtual async Task HandleAddEventAsync<TAddEventModel>(TAddEventModel eventModel) where TAddEventModel : EventModel
        {
            //var idType = typeof(TAddEventModel).GetProperty("Id") ?? typeof(TAddEventModel).GetProperty("id");
            //if (idType != null) await _redis.DelAsync($"{eventModel.GetType().Name.Replace("AddEventModel", "")}Ids");
            await _redis.DelAsync($"{eventModel.GetType().Name.Replace("AddEventModel", "")}Ids");
            await _mongo.AddAsync(new DefaultMongoModel { Name = eventModel.EventModelName });
            await _rabbitMQ.SendMessageAsync(eventModel, queueName: nameof(eventModel));
        }

        /// <summary>
        /// 修改事件处理
        /// </summary>
        /// <typeparam name="TUpdateEventModel">泛型修改事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        /// <returns></returns>
        public virtual async Task HandleUpdateEventAsync<TUpdateEventModel>(TUpdateEventModel eventModel) where TUpdateEventModel : EventModel
        {
            //var idType = typeof(TUpdateEventModel).GetProperty("Id") ?? typeof(TUpdateEventModel).GetProperty("id");
            //if (idType != null) await _redis.DelAsync($"{eventModel.GetType().Name.Replace("UpdateEventModel", "")}Id:{idType.GetValue(eventModel)}");
            await _redis.DelAsync($"{eventModel.GetType().Name.Replace("UpdateEventModel", "")}Id:{eventModel.AggregateId}");
            await _mongo.AddAsync(new DefaultMongoModel { Name = eventModel.EventModelName });
            await _rabbitMQ.SendMessageAsync(eventModel, queueName: nameof(eventModel));
        }

        /// <summary>
        /// 删除事件处理
        /// </summary>
        /// <typeparam name="TDeleteEventModel">泛型删除事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        /// <returns></returns>
        public virtual async Task HandleDeleteEventAsync<TDeleteEventModel>(TDeleteEventModel eventModel) where TDeleteEventModel : EventModel
        {
            //var idType = typeof(TDeleteEventModel).GetProperty("Id") ?? typeof(TDeleteEventModel).GetProperty("id");
            //if (idType != null)
            //{
            //    await _redis.DelAsync($"{eventModel.GetType().Name.Replace("DeleteEventModel", "")}Ids");
            //    await _redis.DelAsync($"{eventModel.GetType().Name.Replace("DeleteEventModel", "")}Id:{idType.GetValue(eventModel)}");
            //}
            await _redis.DelAsync($"{eventModel.GetType().Name.Replace("DeleteEventModel", "")}Ids");
            await _redis.DelAsync($"{eventModel.GetType().Name.Replace("DeleteEventModel", "")}Id:{eventModel.AggregateId}");
            await _rabbitMQ.SendMessageAsync(eventModel, queueName: nameof(eventModel));
        }
    }
}
