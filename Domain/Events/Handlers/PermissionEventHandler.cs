﻿using Domain.Events.Models;
using Domain.IMongos;
using Domain.IRabbitMQs;
using Domain.IRedises;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Events.Handlers
{
    /// <summary>
    /// 权限事件处理
    /// </summary>
    public class PermissionEventHandler : EventHandler<IDefaultRedis, IDefaultMongo, IDefaultRabbitMQ>, INotificationHandler<PermissionAddEventModel>, INotificationHandler<PermissionUpdateEventModel>, INotificationHandler<PermissionDeleteEventModel>
    {
        public PermissionEventHandler(IDefaultRedis redis, IDefaultMongo mongo, IDefaultRabbitMQ rabbitMQ) : base(redis, mongo, rabbitMQ)
        {

        }

        /// <summary>
        ///  新增事件处理
        /// </summary>
        /// <param name="eventModel">新增事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(PermissionAddEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleAddEventAsync(eventModel);
        }

        /// <summary>
        ///  修改事件处理
        /// </summary>
        /// <param name="eventModel">修改事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(PermissionUpdateEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleUpdateEventAsync(eventModel);
        }

        /// <summary>
        /// 删除事件处理
        /// </summary>
        /// <param name="eventModel">删除事件模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(PermissionDeleteEventModel eventModel, CancellationToken cancellationToken)
        {
            await HandleDeleteEventAsync(eventModel);
        }
    }
}
