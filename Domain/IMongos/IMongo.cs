﻿using Domain.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.IMongos
{
    /// <summary>
    /// Mongo接口
    /// </summary>
    public interface IMongo
    {
        #region 同步方法
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="filter">筛选条件</param>
        /// <returns></returns>
        List<TMongoModel> GetList<TMongoModel>(Expression<Func<TMongoModel, bool>> filter) where TMongoModel : MongoModel;

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="filter">筛选条件</param>
        /// <returns></returns>
        List<TMongoModel> GetList<TMongoModel>(FilterDefinition<TMongoModel> filter = null) where TMongoModel : MongoModel;

        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        TMongoModel GetById<TMongoModel>(object id) where TMongoModel : MongoModel;

        /// <summary>
        /// 新增
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="mongoModel">Mongo模型</param>
        void Add<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel;

        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="mongoModel">Mongo模型</param>
        void Update<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel;

        /// <summary>
        /// 根据ID删除
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="id">ID</param>
        void DeleteById<TMongoModel>(object id) where TMongoModel : MongoModel;
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="filter">筛选条件</param>
        /// <returns></returns>
        Task<List<TMongoModel>> GetListAsync<TMongoModel>(Expression<Func<TMongoModel, bool>> filter) where TMongoModel : MongoModel;

        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="filter">筛选条件</param>
        /// <returns></returns>
        Task<List<TMongoModel>> GetListAsync<TMongoModel>(FilterDefinition<TMongoModel> filter = null) where TMongoModel : MongoModel;

        /// <summary>
        /// 根据ID异步查询
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task<TMongoModel> GetByIdAsync<TMongoModel>(object id) where TMongoModel : MongoModel;

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="mongoModel">Mongo模型</param>
        /// <returns></returns>
        Task AddAsync<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel;

        /// <summary>
        /// 异步修改
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="mongoModel">Mongo模型</param>
        /// <returns></returns>
        Task UpdateAsync<TMongoModel>(TMongoModel mongoModel) where TMongoModel : MongoModel;

        /// <summary>
        /// 根据ID异步删除
        /// </summary>
        /// <typeparam name="TMongoModel">泛型Mongo模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task DeleteByIdAsync<TMongoModel>(object id) where TMongoModel : MongoModel;
        #endregion
    }
}
