﻿namespace Domain.MongoModels
{
    /// <summary>
    /// 默认Mongo模型
    /// </summary>
    public class DefaultMongoModel : MongoModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }
    }
}
