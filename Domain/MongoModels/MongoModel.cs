﻿using MongoDB.Bson;
using System;

namespace Domain.MongoModels
{
    /// <summary>
    /// Mongo模型
    /// </summary>
    public abstract class MongoModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public ObjectId Id { get; protected set; } = ObjectId.GenerateNewId();

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; protected set; } = DateTime.Now;

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; protected set; } = DateTime.Now;
    }
}
