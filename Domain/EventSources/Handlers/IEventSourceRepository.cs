﻿using Domain.Events.Models;
using Domain.EventSources.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.EventSources.Handlers
{
    /// <summary>
    /// 事件溯源模型仓储接口
    /// </summary>
    public interface IEventSourceRepository : IDisposable
    {
        #region 同步方法
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TEventSourceModel">泛型事件溯源模型</typeparam>
        /// <param name="aggregateId">聚合根ID</param>
        /// <returns></returns>
        List<TEventSourceModel> GetList<TEventSourceModel>(object aggregateId) where TEventSourceModel : EventSourceModel;

        /// <summary>
        /// 新增
        /// </summary>
        /// <typeparam name="TEventSourceModel">泛型事件溯源模型</typeparam>
        /// <param name="eventSourceModel">事件溯源模型</param>
        void Add<TEventSourceModel>(TEventSourceModel eventSourceModel) where TEventSourceModel : EventSourceModel;

        /// <summary>
        /// 根据事件新增
        /// </summary>
        /// <typeparam name="TEventModel">泛型事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        void AddByEvent<TEventModel>(TEventModel eventModel) where TEventModel : EventModel;
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <typeparam name="TEventSourceModel">泛型事件溯源模型</typeparam>
        /// <param name="aggregateId">聚合根ID</param>
        /// <returns></returns>
        Task<List<TEventSourceModel>> GetListAsync<TEventSourceModel>(object aggregateId) where TEventSourceModel : EventSourceModel;

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <typeparam name="TEventSourceModel">泛型事件溯源模型</typeparam>
        /// <param name="eventSourceModel">事件溯源模型</param>
        /// <returns></returns>
        Task AddAsync<TEventSourceModel>(TEventSourceModel eventSourceModel) where TEventSourceModel : EventSourceModel;

        /// <summary>
        /// 根据事件异步新增
        /// </summary>
        /// <typeparam name="TEventModel">泛型事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        /// <returns></returns>
        Task AddByEventAsync<TEventModel>(TEventModel eventModel) where TEventModel : EventModel;
        #endregion
    }
}
