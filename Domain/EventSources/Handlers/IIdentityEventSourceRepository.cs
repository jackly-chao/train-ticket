﻿namespace Domain.EventSources.Handlers
{
    /// <summary>
    /// 认证事件溯源模型仓储接口
    /// </summary>
    public interface IIdentityEventSourceRepository : IEventSourceRepository
    {

    }
}
