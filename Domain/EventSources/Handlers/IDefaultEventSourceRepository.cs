﻿namespace Domain.EventSources.Handlers
{
    /// <summary>
    /// 默认事件溯源模型仓储接口
    /// </summary>
    public interface IDefaultEventSourceRepository : IEventSourceRepository
    {

    }
}
