﻿using Domain.Events.Models;
using System;

namespace Domain.EventSources.Models
{
    /// <summary>
    /// 事件溯源模型
    /// </summary>
    public class EventSourceModel : EventModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get;  set; }= Guid.NewGuid();

        /// <summary>
        /// 事件数据
        /// </summary>
        public string EventModelData { get;  set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string Operator { get;  set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public new DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
