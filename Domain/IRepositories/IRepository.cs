﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.IRepositories
{
    /// <summary>
    /// 仓储接口
    /// </summary>
    public interface IRepository : IDisposable
    {
        #region 同步方法
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <returns></returns>
        List<TDomainModel> GetList<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel;

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <returns></returns>
        List<TDomainModel> GetList<TDomainModel>() where TDomainModel : DomainModel;

        /// <summary>
        /// 查询
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <returns></returns>
        TDomainModel Get<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel;

        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        TDomainModel GetById<TDomainModel>(object id) where TDomainModel : DomainModel;

        /// <summary>
        /// 新增
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="domainModel">领域模型</param>
        void Add<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel;

        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="domainModel">领域模型</param>
        void Update<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel;

        /// <summary>
        /// 根据ID删除
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="id">ID</param>
        void DeleteById<TDomainModel>(object id) where TDomainModel : DomainModel;

        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        int Save();
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <returns></returns>
        Task<List<TDomainModel>> GetListAsync<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel;

        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <returns></returns>
        Task<List<TDomainModel>> GetListAsync<TDomainModel>() where TDomainModel : DomainModel;

        /// <summary>
        /// 异步查询
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <returns></returns>
        Task<TDomainModel> GetAsync<TDomainModel>(Expression<Func<TDomainModel, bool>> predicate) where TDomainModel : DomainModel;

        /// <summary>
        /// 根据ID异步查询
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task<TDomainModel> GetByIdAsync<TDomainModel>(object id) where TDomainModel : DomainModel;

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="domainModel">领域模型</param>
        /// <returns></returns>
        Task AddAsync<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel;

        /// <summary>
        /// 异步修改
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="domainModel">领域模型</param>
        /// <returns></returns>
        Task UpdateAsync<TDomainModel>(TDomainModel domainModel) where TDomainModel : DomainModel;

        /// <summary>
        /// 根据ID异步删除
        /// </summary>
        /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task DeleteByIdAsync<TDomainModel>(object id) where TDomainModel : DomainModel;

        /// <summary>
        /// 异步保存
        /// </summary>
        /// <returns></returns>
        Task<int> SaveAsync();
        #endregion
    }
}
