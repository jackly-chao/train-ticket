﻿using Domain.Models;
using System.Threading.Tasks;

namespace Domain.IRepositories
{
    /// <summary>
    /// 默认仓储接口
    /// </summary>
    public interface IDefaultRepository : IRepository
    {
        /// <summary>
        /// 根据中文名异步查询
        /// </summary>
        /// <param name="name">中文名</param>
        /// <returns></returns>
        Task<DefaultDomainModel> GetByNameAsync(string name);

        /// <summary>
        /// 根据英文名异步查询
        /// </summary>
        /// <param name="enName">英文名</param>
        /// <returns></returns>
        Task<DefaultDomainModel> GetByEnNameAsync(string enName);
    }
}
