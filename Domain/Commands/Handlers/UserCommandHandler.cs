﻿using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.Events.Models;
using Domain.IRepositories;
using Domain.IUnitOfWorks;
using Domain.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Commands.Handlers
{
    /// <summary>
    /// 用户命令处理
    /// </summary>
    public class UserCommandHandler : CommandHandler<IIdentityUnitOfWork, IIdentityBus, IIdentityRepository>, IRequestHandler<UserLoginCommandModel, bool>
    {
        public UserCommandHandler(IIdentityUnitOfWork unitOfWork, IIdentityBus bus, IIdentityRepository repository) : base(unitOfWork, bus, repository)
        {

        }

        /// <summary>
        /// 登录命令处理
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(UserLoginCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            var oldUser = await _repository.GetAsync<UserDomainModel>(d => d.UserName == request.UserName);
            //判断
            if (oldUser == null)
            {
                await HandleFailedAsync(request, "该用户名不存在!");
                return await Task.FromResult(false);
            }
            if (!oldUser.IsOpen())
            {
                await HandleFailedAsync(request, "该用户已禁止登录!");
                return await Task.FromResult(false);
            }
            if (oldUser.Password != request.Password)
            {
                await HandleFailedAsync(request, "密码错误!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new UserLoginEventModel(oldUser.Id, oldUser.UserName));
            return await Task.FromResult(true);
        }
    }
}
