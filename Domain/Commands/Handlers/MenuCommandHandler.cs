﻿using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.Events.Models;
using Domain.IRepositories;
using Domain.IUnitOfWorks;
using Domain.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Commands.Handlers
{
    /// <summary>
    /// 菜单命令处理程序
    /// </summary>
    public class MenuCommandHandler : CommandHandler<IDefaultUnitOfWork, IDefaultBus, IDefaultRepository>, IRequestHandler<MenuAddCommandModel, bool>, IRequestHandler<MenuUpdateCommandModel, bool>, IRequestHandler<MenuDeleteCommandModel, bool>
    {
        public MenuCommandHandler(IDefaultUnitOfWork unitOfWork, IDefaultBus bus, IDefaultRepository repository) : base(unitOfWork, bus, repository)
        {

        }

        /// <summary>
        /// 新增命令处理
        /// </summary>
        /// <param name="request">新增命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(MenuAddCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            //判断
            if (await _repository.GetAsync<MenuDomainModel>(d => d.Name == request.Name) != null)
            {
                await HandleFailedAsync(request, "该中文名已被使用!");
                return await Task.FromResult(false);
            }
            if (await _repository.GetAsync<MenuDomainModel>(d => d.EnName == request.EnName) != null)
            {
                await HandleFailedAsync(request, "该英文名已被使用!");
                return await Task.FromResult(false);
            }
            var data = new MenuDomainModel(0, request.Name, request.EnName, request.Status);
            await _repository.AddAsync(data);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "新增失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new MenuAddEventModel(data.Id, data.Name, data.EnName, request.Status));
            return await Task.FromResult(true);
        }

        /// <summary>
        /// 修改命令处理
        /// </summary>
        /// <param name="request">修改命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(MenuUpdateCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            //判断
            if (await _repository.GetAsync<MenuDomainModel>(d => d.Name == request.Name && d.Id != request.Id) != null)
            {
                await HandleFailedAsync(request, "该中文名已被使用!");
                return await Task.FromResult(false);
            }
            if (await _repository.GetAsync<MenuDomainModel>(d => d.EnName == request.EnName && d.Id != request.Id) != null)
            {
                await HandleFailedAsync(request, "该英文名已被使用!");
                return await Task.FromResult(false);
            }
            var data = new MenuDomainModel(request.Id, request.Name, request.EnName, request.Status);
            await _repository.UpdateAsync(data);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "修改失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new MenuUpdateEventModel(data.Id, data.Name, data.EnName, request.Status));
            return await Task.FromResult(true);
        }

        /// <summary>
        /// 删除命令处理
        /// </summary>
        /// <param name="request">删除命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(MenuDeleteCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            await _repository.DeleteByIdAsync<MenuDomainModel>(request.Id);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "删除失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new MenuDeleteEventModel(request.Id));
            return await Task.FromResult(true);
        }
    }
}
