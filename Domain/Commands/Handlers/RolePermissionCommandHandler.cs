﻿using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.Events.Models;
using Domain.IRepositories;
using Domain.IUnitOfWorks;
using Domain.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Commands.Handlers
{
    /// <summary>
    /// 角色权限命令处理程序
    /// </summary>
    public class RolePermissionCommandHandler : CommandHandler<IDefaultUnitOfWork, IDefaultBus, IDefaultRepository>, IRequestHandler<RolePermissionAddCommandModel, bool>, IRequestHandler<RolePermissionUpdateCommandModel, bool>, IRequestHandler<RolePermissionDeleteCommandModel, bool>
    {
        public RolePermissionCommandHandler(IDefaultUnitOfWork unitOfWork, IDefaultBus bus, IDefaultRepository repository) : base(unitOfWork, bus, repository)
        {

        }

        /// <summary>
        /// 新增命令处理
        /// </summary>
        /// <param name="request">新增命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(RolePermissionAddCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            //判断
            if (await _repository.GetAsync<RolePermissionDomainModel>(d => d.RoleId == request.RoleId && d.PermissionId == request.PermissionId) != null)
            {
                await HandleFailedAsync(request, "该角色权限已存在!");
                return await Task.FromResult(false);
            }
            var data = new RolePermissionDomainModel(0, request.RoleId, request.PermissionId, request.Status);
            await _repository.AddAsync(data);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "新增失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new RolePermissionAddEventModel(data.Id, data.RoleId, data.PermissionId, request.Status));
            return await Task.FromResult(true);
        }

        /// <summary>
        /// 修改命令处理
        /// </summary>
        /// <param name="request">修改命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(RolePermissionUpdateCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            //判断
            if (await _repository.GetAsync<RolePermissionDomainModel>(d => d.RoleId == request.RoleId && d.PermissionId == request.PermissionId) != null)
            {
                await HandleFailedAsync(request, "该角色权限已存在!");
                return await Task.FromResult(false);
            }
            var data = new RolePermissionDomainModel(0, request.RoleId, request.PermissionId, request.Status);
            await _repository.AddAsync(data);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "修改失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new RolePermissionUpdateEventModel(data.Id, data.RoleId, data.PermissionId, request.Status));
            return await Task.FromResult(true);
        }

        /// <summary>
        /// 删除命令处理
        /// </summary>
        /// <param name="request">删除命令模型</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(RolePermissionDeleteCommandModel request, CancellationToken cancellationToken)
        {
            //验证
            if (!request.IsValid())
            {
                await HandleValidateFailedAsync(request);
                return await Task.FromResult(false);
            }
            await _repository.DeleteByIdAsync<RolePermissionDomainModel>(request.Id);
            //提交才会真正保存到数据库
            if (!await CommitAsync())
            {
                await HandleFailedAsync(request, "删除失败!");
                return await Task.FromResult(false);
            }
            await HandleSucceedAsync(new RolePermissionDeleteEventModel(request.Id));
            return await Task.FromResult(true);
        }
    }
}
