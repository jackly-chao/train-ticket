﻿using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.Events.Models;
using Domain.IRepositories;
using Domain.IUnitOfWorks;
using Domain.Notifications.Models;
using System.Threading.Tasks;

namespace Domain.Commands.Handlers
{
    /// <summary>
    /// 命令处理
    /// </summary>
    /// <typeparam name="TIUnitOfWork">泛型工作单元接口</typeparam>
    /// <typeparam name="TIBus">泛型总线(中介者)处理接口</typeparam>
    /// <typeparam name="TIRepository">泛型仓储接口</typeparam>
    public abstract class CommandHandler<TIUnitOfWork, TIBus, TIRepository> where TIUnitOfWork : IUnitOfWork where TIBus : IBus where TIRepository : IRepository
    {
        /// <summary>
        /// 工作单元接口
        /// </summary>
        protected readonly TIUnitOfWork _unitOfWork;

        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        protected readonly TIBus _bus;

        /// <summary>
        /// 仓储接口
        /// </summary>
        protected readonly TIRepository _repository;

        public CommandHandler(TIUnitOfWork unitOfWork, TIBus bus, TIRepository repository)
        {
            _unitOfWork = unitOfWork;
            _bus = bus;
            _repository = repository;
        }

        /// <summary>
        /// 工作单元提交
        /// </summary>
        /// <param name="byTransaction">是否通过事务提交，true：是，false：否</param>
        /// <returns></returns>
        public async Task<bool> CommitAsync(bool byTransaction = false)
        {
            if (byTransaction) return await _unitOfWork.CommitByTransactionAsync() > 0;
            return await _unitOfWork.CommitAsync() > 0;
        }

        /// <summary>
        /// 命令模型验证失败,触发通知,发布到总线
        /// </summary>
        /// <typeparam name="TCommandModel">泛型命令模型</typeparam>
        /// <param name="commandModel">命令模型</param>
        /// <returns></returns>
        public async Task HandleValidateFailedAsync<TCommandModel>(TCommandModel commandModel) where TCommandModel : CommandModel
        {
            foreach (var error in commandModel.ValidationResult.Errors)
            {
                await _bus.RaiseNotificationAsync(new DefaultNotificationModel { Key = commandModel.CommandModelName, Value = error.ErrorMessage });
            }
        }

        /// <summary>
        /// 命令模型处理成功,触发事件,发布到总线
        /// </summary>
        /// <typeparam name="TEventModel">泛型事件模型</typeparam>
        /// <param name="eventModel">事件模型</param>
        /// <returns></returns>
        public async Task HandleSucceedAsync<TEventModel>(TEventModel eventModel) where TEventModel : EventModel
        {
            await _bus.RaiseEventAsync(eventModel);
        }

        /// <summary>
        /// 命令模型处理失败,触发通知,发布到总线
        /// </summary>
        /// <typeparam name="TNotificationModel">泛型通知模型</typeparam>
        /// <param name="notificationModel">通知模型</param>
        /// <returns></returns>
        public async Task HandleFailedAsync<TNotificationModel>(TNotificationModel notificationModel) where TNotificationModel : NotificationModel
        {
            await _bus.RaiseNotificationAsync(notificationModel);
        }

        /// <summary>
        /// 命令模型处理失败,触发通知,发布到总线
        /// </summary>
        /// <typeparam name="TCommandModel">泛型命令模型</typeparam>
        /// <param name="commandModel">命令模型</param>
        /// <param name="faileMessage">失败信息</param>
        /// <returns></returns>
        public async Task HandleFailedAsync<TCommandModel>(TCommandModel commandModel, string faileMessage) where TCommandModel : CommandModel
        {
            await _bus.RaiseNotificationAsync(new DefaultNotificationModel { Key = commandModel.GetType().Name, Value = faileMessage });
        }
    }
}
