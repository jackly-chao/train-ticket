﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 菜单命令模型
    /// </summary>
    public abstract class MenuCommandModel : CommandModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 菜单命令模型验证
    /// </summary>
    /// <typeparam name="TMenuCommandModel">泛型菜单命令模型</typeparam>
    public class MenuCommandModelValidation<TMenuCommandModel> : AbstractValidator<TMenuCommandModel> where TMenuCommandModel : MenuCommandModel
    {
        /// <summary>
        /// 验证ID
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(d => d.Id)
                .GreaterThan(0).WithMessage("ID应大于0");
        }

        /// <summary>
        /// 验证中文名
        /// </summary>
        protected void ValidateName()
        {
            RuleFor(d => d.Name)
                .NotEmpty().WithMessage("中文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("中文名在2~10个字符之间");//定义长度
        }

        /// <summary>
        /// 验证英文名
        /// </summary>
        protected void ValidateEnName()
        {
            RuleFor(d => d.EnName)
                .NotEmpty().WithMessage("英文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("英文名在2~10个字符之间");//定义长度
        }
    }

    /// <summary>
    /// 菜单新增命令模型
    /// </summary>
    public class MenuAddCommandModel : MenuCommandModel
    {
        public MenuAddCommandModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new MenuAddCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 菜单新增命令模型验证
    /// </summary>
    public class MenuAddCommandModelValidation : MenuCommandModelValidation<MenuAddCommandModel>
    {
        public MenuAddCommandModelValidation()
        {
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 菜单修改命令模型
    /// </summary>
    public class MenuUpdateCommandModel : MenuCommandModel
    {
        public MenuUpdateCommandModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new MenuUpdateCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 菜单修改命令模型验证
    /// </summary>
    public class MenuUpdateCommandModelValidation : MenuCommandModelValidation<MenuUpdateCommandModel>
    {
        public MenuUpdateCommandModelValidation()
        {
            ValidateId();
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 菜单删除命令模型
    /// </summary>
    public class MenuDeleteCommandModel : MenuCommandModel
    {
        public MenuDeleteCommandModel(int id)
        {
            AggregateId = id;
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new MenuDeleteCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 菜单删除命令模型验证
    /// </summary>
    public class MenuDeleteCommandModelValidation : MenuCommandModelValidation<MenuDeleteCommandModel>
    {
        public MenuDeleteCommandModelValidation()
        {
            ValidateId();
        }
    }
}
