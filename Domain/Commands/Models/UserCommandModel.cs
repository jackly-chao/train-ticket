﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 用户命令模型
    /// </summary>
    public abstract class UserCommandModel : CommandModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; protected set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; protected set; }
    }

    /// <summary>
    /// 用户命令模型验证
    /// </summary>
    /// <typeparam name="TUserCommandModel">泛型用户命令模型</typeparam>
    public class UserCommandModelValidation<TUserCommandModel> : AbstractValidator<TUserCommandModel> where TUserCommandModel : UserCommandModel
    {
        /// <summary>
        /// 验证用户名
        /// </summary>
        protected void ValidateUserName()
        {
            RuleFor(d => d.UserName)
                .NotEmpty().WithMessage("用户名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("用户名在2~50个字符之间");//定义长度
        }

        /// <summary>
        /// 验证密码
        /// </summary>
        protected void ValidatePassword()
        {
            RuleFor(d => d.Password)
                .NotEmpty().WithMessage("密码不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("密码在2~50个字符之间");//定义长度
        }
    }

    /// <summary>
    /// 用户登录命令模型
    /// </summary>
    public class UserLoginCommandModel : UserCommandModel
    {
        public UserLoginCommandModel(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        public override bool IsValid()
        {
            ValidationResult = new UserLoginCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 用户登录命令模型验证
    /// </summary>
    public class UserLoginCommandModelValidation : UserCommandModelValidation<UserLoginCommandModel>
    {
        public UserLoginCommandModelValidation()
        {
            ValidateUserName();
            ValidatePassword();
        }
    }
}
