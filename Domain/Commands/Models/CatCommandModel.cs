﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 猫咪命令模型
    /// </summary>
    public abstract class CatCommandModel : CommandModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 猫咪命令模型验证
    /// </summary>
    /// <typeparam name="TCatCommandModel">泛型猫咪命令模型</typeparam>
    public class CatCommandModelValidation<TCatCommandModel> : AbstractValidator<TCatCommandModel> where TCatCommandModel : CatCommandModel
    {
        /// <summary>
        /// 验证ID
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(d => d.Id)
                .GreaterThan(0).WithMessage("ID应大于0");
        }

        /// <summary>
        /// 验证中文名
        /// </summary>
        protected void ValidateName()
        {
            RuleFor(d => d.Name)
                .NotEmpty().WithMessage("中文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("中文名在2~10个字符之间");//定义长度
        }

        /// <summary>
        /// 验证英文名
        /// </summary>
        protected void ValidateEnName()
        {
            RuleFor(d => d.EnName)
                .NotEmpty().WithMessage("英文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("英文名在2~10个字符之间");//定义长度
        }
    }

    /// <summary>
    /// 猫咪新增命令模型
    /// </summary>
    public class CatAddCommandModel : CatCommandModel
    {
        public CatAddCommandModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new CatAddCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 猫咪新增命令模型验证
    /// </summary>
    public class CatAddCommandModelValidation : CatCommandModelValidation<CatAddCommandModel>
    {
        public CatAddCommandModelValidation()
        {
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 猫咪修改命令模型
    /// </summary>
    public class CatUpdateCommandModel : CatCommandModel
    {
        public CatUpdateCommandModel(int id, string name, string enName, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new CatUpdateCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 猫咪修改命令模型验证
    /// </summary>
    public class CatUpdateCommandModelValidation : CatCommandModelValidation<CatUpdateCommandModel>
    {
        public CatUpdateCommandModelValidation()
        {
            ValidateId();
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 猫咪删除命令模型
    /// </summary>
    public class CatDeleteCommandModel : CatCommandModel
    {
        public CatDeleteCommandModel(int id)
        {
            AggregateId = id;
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new CatDeleteCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 猫咪删除命令模型验证
    /// </summary>
    public class CatDeleteCommandModelValidation : CatCommandModelValidation<CatDeleteCommandModel>
    {
        public CatDeleteCommandModelValidation()
        {
            ValidateId();
        }
    }
}
