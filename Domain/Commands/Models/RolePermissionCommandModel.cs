﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 角色权限命令模型
    /// </summary>
    public abstract class RolePermissionCommandModel : CommandModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; protected set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 角色权限命令模型验证
    /// </summary>
    /// <typeparam name="TRolePermissionCommandModel">泛型角色权限命令模型</typeparam>
    public class RolePermissionCommandModelValidation<TRolePermissionCommandModel> : AbstractValidator<TRolePermissionCommandModel> where TRolePermissionCommandModel : RolePermissionCommandModel
    {
        /// <summary>
        /// 验证ID
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(d => d.Id)
                .GreaterThan(0).WithMessage("ID应大于0");
        }

        /// <summary>
        /// 验证角色ID
        /// </summary>
        protected void ValidateRoleId()
        {
            RuleFor(d => d.RoleId)
                .GreaterThan(0).WithMessage("角色ID应大于0");
        }

        /// <summary>
        /// 验证权限ID
        /// </summary>
        protected void ValidatePermissionId()
        {
            RuleFor(d => d.PermissionId)
               .GreaterThan(0).WithMessage("权限ID应大于0");
        }
    }

    /// <summary>
    /// 角色权限新增命令模型
    /// </summary>
    public class RolePermissionAddCommandModel : RolePermissionCommandModel
    {
        public RolePermissionAddCommandModel(int id, int roleId, int permissionId, int status)
        {
            AggregateId = id;
            Id = id;
            RoleId = roleId;
            PermissionId = permissionId;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new RolePermissionAddCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 角色权限新增命令模型验证
    /// </summary>
    public class RolePermissionAddCommandModelValidation : RolePermissionCommandModelValidation<RolePermissionAddCommandModel>
    {
        public RolePermissionAddCommandModelValidation()
        {
            ValidateRoleId();
            ValidatePermissionId();
        }
    }

    /// <summary>
    /// 角色权限修改命令模型
    /// </summary>
    public class RolePermissionUpdateCommandModel : RolePermissionCommandModel
    {
        public RolePermissionUpdateCommandModel(int id, int roleId, int permissionId, int status)
        {
            AggregateId = id;
            Id = id;
            RoleId = roleId;
            PermissionId = permissionId;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new RolePermissionUpdateCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 角色权限修改命令模型验证
    /// </summary>
    public class RolePermissionUpdateCommandModelValidation : RolePermissionCommandModelValidation<RolePermissionUpdateCommandModel>
    {
        public RolePermissionUpdateCommandModelValidation()
        {
            ValidateId();
            ValidateRoleId();
            ValidatePermissionId();
        }
    }

    /// <summary>
    /// 角色权限删除命令模型
    /// </summary>
    public class RolePermissionDeleteCommandModel : RolePermissionCommandModel
    {
        public RolePermissionDeleteCommandModel(int id)
        {
            AggregateId = id;
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RolePermissionDeleteCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 角色权限删除命令模型验证
    /// </summary>
    public class RolePermissionDeleteCommandModelValidation : RolePermissionCommandModelValidation<RolePermissionDeleteCommandModel>
    {
        public RolePermissionDeleteCommandModelValidation()
        {
            ValidateId();
        }
    }
}
