﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 权限命令模型
    /// </summary>
    public abstract class PermissionCommandModel : CommandModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }

        /// <summary>
        /// 路由
        /// </summary>
        public string Route { get; protected set; }

        /// <summary>
        /// 请求方法
        /// </summary>
        public int Method { get; protected set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuId { get; protected set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; protected set; }
    }

    /// <summary>
    /// 权限命令模型验证
    /// </summary>
    /// <typeparam name="TPermissionCommandModel">泛型权限命令模型</typeparam>
    public class PermissionCommandModelValidation<TPermissionCommandModel> : AbstractValidator<TPermissionCommandModel> where TPermissionCommandModel : PermissionCommandModel
    {
        /// <summary>
        /// 验证ID
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(d => d.Id)
                .GreaterThan(0).WithMessage("ID应大于0");
        }

        /// <summary>
        /// 验证中文名
        /// </summary>
        protected void ValidateName()
        {
            RuleFor(d => d.Name)
                .NotEmpty().WithMessage("中文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("中文名在2~50个字符之间");//定义长度
        }

        /// <summary>
        /// 验证英文名
        /// </summary>
        protected void ValidateEnName()
        {
            RuleFor(d => d.EnName)
                .NotEmpty().WithMessage("英文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("英文名在2~50个字符之间");//定义长度
        }

        /// <summary>
        /// 验证路由
        /// </summary>
        protected void ValidateRoute()
        {
            RuleFor(d => d.Route)
                .NotEmpty().WithMessage("路由不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 50).WithMessage("路由在2~50个字符之间");//定义长度
        }

        /// <summary>
        /// 验证请求方法
        /// </summary>
        protected void ValidateMethod()
        {
            RuleFor(d => d.Method)
                .GreaterThan(0).WithMessage("请求方法应大于0");
        }

        /// <summary>
        /// 验证菜单ID
        /// </summary>
        protected void ValidateMenuId()
        {
            RuleFor(d => d.MenuId)
                .GreaterThan(0).WithMessage("菜单ID应大于0");
        }
    }

    /// <summary>
    /// 权限新增命令模型
    /// </summary>
    public class PermissionAddCommandModel : PermissionCommandModel
    {
        public PermissionAddCommandModel(int id, string name, string enName, string route, int method, int menuId, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Route = route;
            Method = method;
            MenuId = menuId;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new PermissionAddCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 权限新增命令模型验证
    /// </summary>
    public class PermissionAddCommandModelValidation : PermissionCommandModelValidation<PermissionAddCommandModel>
    {
        public PermissionAddCommandModelValidation()
        {
            ValidateName();
            ValidateEnName();
            ValidateRoute();
            ValidateMethod();
            ValidateMenuId();
        }
    }

    /// <summary>
    /// 权限修改命令模型
    /// </summary>
    public class PermissionUpdateCommandModel : PermissionCommandModel
    {
        public PermissionUpdateCommandModel(int id, string name, string enName, string route, int method, int menuId, int status)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
            Route = route;
            Method = method;
            MenuId = menuId;
            Status = status;
        }

        public override bool IsValid()
        {
            ValidationResult = new PermissionUpdateCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 权限修改命令模型验证
    /// </summary>
    public class PermissionUpdateCommandModelValidation : PermissionCommandModelValidation<PermissionUpdateCommandModel>
    {
        public PermissionUpdateCommandModelValidation()
        {
            ValidateId();
            ValidateName();
            ValidateEnName();
            ValidateRoute();
            ValidateMethod();
            ValidateMenuId();
        }
    }

    /// <summary>
    /// 权限删除命令模型
    /// </summary>
    public class PermissionDeleteCommandModel : PermissionCommandModel
    {
        public PermissionDeleteCommandModel(int id)
        {
            AggregateId = id;
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new PermissionDeleteCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 权限删除命令模型验证
    /// </summary>
    public class PermissionDeleteCommandModelValidation : PermissionCommandModelValidation<PermissionDeleteCommandModel>
    {
        public PermissionDeleteCommandModelValidation()
        {
            ValidateId();
        }
    }
}
