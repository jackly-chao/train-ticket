﻿using FluentValidation;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 默认命令模型
    /// </summary>
    public abstract class DefaultCommandModel : CommandModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; protected set; }
    }

    /// <summary>
    /// 默认命令模型验证
    /// </summary>
    /// <typeparam name="TDefaultCommandModel">泛型默认命令模型</typeparam>
    public abstract class DefaultCommandModelValidation<TDefaultCommandModel> : CommandModelValidation<TDefaultCommandModel> where TDefaultCommandModel : DefaultCommandModel
    {
        /// <summary>
        /// 验证ID
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(d => d.Id)
                .GreaterThan(0).WithMessage("ID应大于0");
        }

        /// <summary>
        /// 验证中文名
        /// </summary>
        protected void ValidateName()
        {
            RuleFor(d => d.Name)
                .NotEmpty().WithMessage("中文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 10).WithMessage("中文名在2~10个字符之间");//定义长度
        }

        /// <summary>
        /// 验证英文名
        /// </summary>
        protected void ValidateEnName()
        {
            RuleFor(d => d.EnName)
                .NotEmpty().WithMessage("英文名不能为空")//判断不能为空，如果为空则显示Message
                .Length(2, 10).WithMessage("英文名在2~10个字符之间");//定义长度
        }
    }

    /// <summary>
    /// 默认新增命令模型
    /// </summary>
    public class DefaultAddCommandModel : DefaultCommandModel
    {
        public DefaultAddCommandModel(int id, string name, string enName)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
        }

        public override bool IsValid()
        {
            ValidationResult = new DefaultAddCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 默认新增命令模型验证
    /// </summary>
    public class DefaultAddCommandModelValidation : DefaultCommandModelValidation<DefaultAddCommandModel>
    {
        public DefaultAddCommandModelValidation()
        {
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 默认修改命令模型
    /// </summary>
    public class DefaultUpdateCommandModel : DefaultCommandModel
    {
        public DefaultUpdateCommandModel(int id, string name, string enName)
        {
            AggregateId = id;
            Id = id;
            Name = name;
            EnName = enName;
        }

        public override bool IsValid()
        {
            ValidationResult = new DefaultUpdateCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 默认修改命令模型验证
    /// </summary>
    public class DefaultUpdateCommandModelValidation : DefaultCommandModelValidation<DefaultUpdateCommandModel>
    {
        public DefaultUpdateCommandModelValidation()
        {
            ValidateId();
            ValidateName();
            ValidateEnName();
        }
    }

    /// <summary>
    /// 默认删除命令模型
    /// </summary>
    public class DefaultDeleteCommandModel : DefaultCommandModel
    {
        public DefaultDeleteCommandModel(int id)
        {
            AggregateId = id;
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DefaultDeleteCommandModelValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    /// <summary>
    /// 默认删除命令模型验证
    /// </summary>
    public class DefaultDeleteCommandModelValidation : DefaultCommandModelValidation<DefaultDeleteCommandModel>
    {
        public DefaultDeleteCommandModelValidation()
        {
            ValidateId();
        }
    }
}
