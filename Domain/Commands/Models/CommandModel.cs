﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using System;

namespace Domain.Commands.Models
{
    /// <summary>
    /// 命令模型
    /// </summary>
    public abstract class CommandModel : IRequest<bool>
    {
        /// <summary>
        /// 聚合根ID
        /// </summary>
        public int AggregateId { get; protected set; }

        /// <summary>
        /// 命令模型名
        /// </summary>
        public string CommandModelName { get; protected set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; protected set; } = DateTime.Now;

        /// <summary>
        /// 验证结果
        /// </summary>
        public ValidationResult ValidationResult { get; set; }

        /// <summary>
        /// 判断是否有效
        /// </summary>
        /// <returns></returns>
        public abstract bool IsValid();
    }

    /// <summary>
    /// 命令模型验证
    /// </summary>
    public abstract class CommandModelValidation<TCommandModel> : AbstractValidator<TCommandModel> where TCommandModel : CommandModel
    {
        /// <summary>
        /// 验证命令模型名
        /// </summary>
        protected void ValidateCommandModelName()
        {
            RuleFor(d => d.CommandModelName)
                .NotNull().NotEmpty().WithMessage("命令模型名不能为空或空字符串");
        }

        /// <summary>
        /// 验证创建时间
        /// </summary>
        protected void ValidateCreateTime()
        {
            RuleFor(d => d.CreateTime)
                .GreaterThanOrEqualTo(DateTime.Now).WithMessage("创建时间应不晚于当前时间");
        }
    }
}
