﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Helpers
{
    /// <summary>
    /// MD5帮助类
    /// </summary>
    public class MD5Helper
    {
        /// <summary>
        /// 16位加密
        /// </summary>
        /// <param name="password">需要加密的原始密码</param>
        /// <param name="isGetLower">加密后的密码是否是小写（true：是，false：小写）</param>
        /// <returns></returns>
        public static string Encrypt_16(string password, bool isGetLower = false)
        {
            string result = null;
            if (string.IsNullOrWhiteSpace(password)) return result;
            result = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password)), 4, 8);
            result = result.Replace("-", "");
            if (isGetLower) result = result.ToLower();
            return result;
        }

        /// <summary>
        /// 32位加密
        /// </summary>
        /// <param name="password">需要加密的原始密码</param>
        /// <param name="isGetLower">加密后的密码是否是小写（true：是，false：小写）</param>
        /// <returns></returns>
        public static string Encrypt_32(string password, bool isGetLower = false) 
        {
            string result = null;
            if (string.IsNullOrWhiteSpace(password)) return result;
            byte[] data = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder sBuilder = new StringBuilder();
            var format = "X2";
            if (isGetLower) format = "x2";
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString(format));
            }
            result = sBuilder.ToString();
            return result;
        }

        /// <summary>
        /// 64位加密
        /// </summary>
        /// <param name="password">需要加密的原始密码</param>
        /// <returns></returns>
        public static string Encrypt_64(string password)
        {
            string result = null;
            if (string.IsNullOrWhiteSpace(password)) return result;
            byte[] data = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password));
            result= Convert.ToBase64String(data);
            return result;
        }

        /// <summary>
        /// SHA256哈希加密
        /// </summary>
        /// <param name="password">需要加密的原始密码</param>
        /// <returns></returns>
        public static string Encrypt_SHA256Hash(string password)
        {
            string result = null;
            using (var sha = SHA256.Create())
            {
                var computedHash = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
                result = BitConverter.ToString(computedHash).Replace("-", "");
            }
            return result;
        }
    }
}
