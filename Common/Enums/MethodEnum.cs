﻿namespace Common.Enums
{
    /// <summary>
    /// 请求方法枚举
    /// </summary>
    public enum MethodEnum
    {
        /// <summary>
        /// Get
        /// </summary>
        Get = 1,

        /// <summary>
        /// Post
        /// </summary>
        Post = 2,

        /// <summary>
        /// Put
        /// </summary>
        Put = 3,

        /// <summary>
        /// Delete
        /// </summary>
        Delete = 4,
    }
}
