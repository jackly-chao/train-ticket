﻿namespace Common.Enums
{
    /// <summary>
    /// 状态枚举
    /// </summary>
    public enum StatusEnum
    {
        /// <summary>
        /// 删除
        /// </summary>
        Delete = -1,

        /// <summary>
        /// 关闭
        /// </summary>
        Close = 0,

        /// <summary>
        /// 开启
        /// </summary>
        Open = 1
    }
}
