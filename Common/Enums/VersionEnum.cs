﻿namespace Common.Enums
{
    /// <summary>
    /// 版本枚举
    /// </summary>
    public enum VersionEnum
    {
        /// <summary>
        /// V1版本
        /// </summary>
        v1 = 1,

        /// <summary>
        /// V2版本
        /// </summary>
        v2 = 2
    }
}
