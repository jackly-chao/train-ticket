﻿using Domain.Notifications.Models;

namespace Application.INotifications
{
    /// <summary>
    /// 默认通知接口
    /// </summary>
    public interface IDefaultNotification : INotification<DefaultNotificationModel>
    {

    }
}
