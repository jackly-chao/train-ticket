﻿using Domain.Notifications.Models;
using System.Collections.Generic;

namespace Application.INotifications
{
    /// <summary>
    /// 通知接口，其实可以不用这个，只是为了不直接暴露领域层
    /// </summary>
    /// <typeparam name="TNotificationModel">泛型通知模型</typeparam>
    public interface INotification<TNotificationModel> where TNotificationModel : NotificationModel
    {
        /// <summary>
        /// 查询当前生命周期内的通知列表
        /// </summary>
        /// <returns></returns>
        public List<TNotificationModel> GetList(bool clearNotificationAfterGet = true);
        
        /// <summary>
        /// 判断当前生命周期内是否存在通知
        /// </summary>
        /// <returns></returns>
        public bool HasNotifications();
    }
}
