﻿using Application.INotifications;
using Domain.IRedises;
using Domain.Notifications.Handlers;
using Domain.Notifications.Models;

namespace Application.Notifications
{
    /// <summary>
    /// 默认通知
    /// </summary>
    public class DefaultNotification : Notification<DefaultNotificationHandler, DefaultNotificationModel, IDefaultRedis>, IDefaultNotification
    {
        public DefaultNotification(DefaultNotificationHandler notificationHandler) : base(notificationHandler)
        {

        }
    }
}
