﻿using Application.INotifications;
using Domain.IRedises;
using Domain.Notifications.Models;
using System.Collections.Generic;

namespace Application.Notifications
{
    /// <summary>
    /// 通知
    /// </summary>
    /// <typeparam name="TNotificationHandler">泛型通知处理</typeparam>
    /// <typeparam name="TNotificationModel">泛型通知模型</typeparam>
    /// <typeparam name="TIRedis">泛型Redis接口</typeparam>
    public abstract class Notification<TNotificationHandler, TNotificationModel, TIRedis> : INotification<TNotificationModel> where TNotificationHandler : Domain.Notifications.Handlers.NotificationHandler<TNotificationModel, TIRedis> where TNotificationModel : NotificationModel where TIRedis : IRedis
    {
        /// <summary>
        /// 通知处理
        /// </summary>
        protected readonly TNotificationHandler _notificationHandler;

        public Notification(TNotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public List<TNotificationModel> GetList(bool clearNotificationAfterGet = true)
        {
            return _notificationHandler.GetList(clearNotificationAfterGet);
        }

        public bool HasNotifications()
        {
            return _notificationHandler.HasNotifications();
        }
    }
}
