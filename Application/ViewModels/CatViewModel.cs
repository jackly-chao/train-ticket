﻿namespace Application.ViewModels
{
    /// <summary>
    /// 猫咪视图模型
    /// </summary>
    public class CatViewModel : ViewModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }
    }
}
