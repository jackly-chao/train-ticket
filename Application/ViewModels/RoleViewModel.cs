﻿namespace Application.ViewModels
{
    /// <summary>
    /// 角色视图模型
    /// </summary>
    public class RoleViewModel : ViewModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }
    }
}
