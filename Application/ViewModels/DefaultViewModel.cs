﻿namespace Application.ViewModels
{
    /// <summary>
    /// 默认视图模型(DTO)
    /// </summary>
    public class DefaultViewModel : ViewModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }
    }
}
