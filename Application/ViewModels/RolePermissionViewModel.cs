﻿namespace Application.ViewModels
{
    /// <summary>
    /// 角色权限视图模型
    /// </summary>
    public class RolePermissionViewModel : ViewModel
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public int PermissionId { get; set; }
    }
}
