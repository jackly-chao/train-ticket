﻿namespace Application.ViewModels
{
    /// <summary>
    /// 用户角色视图模型
    /// </summary>
    public class UserRoleViewModel : ViewModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
    }
}
