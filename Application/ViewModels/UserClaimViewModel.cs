﻿namespace Application.ViewModels
{
    /// <summary>
    /// 用户声明视图模型
    /// </summary>
    public class UserClaimViewModel : ViewModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 声明类型
        /// </summary>
        public string ClaimType { get; set; }

        /// <summary>
        /// 声明值
        /// </summary>
        public string ClaimValue { get; set; }
    }
}
