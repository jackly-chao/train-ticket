﻿namespace Application.ViewModels
{
    /// <summary>
    /// 权限视图模型
    /// </summary>
    public class PermissionViewModel : ViewModel
    {
        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string EnName { get; set; }

        /// <summary>
        /// 路由
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// 请求方法
        /// </summary>
        public int Method { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuId { get; set; }
    }
}
