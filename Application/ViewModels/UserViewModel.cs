﻿using Common.Enums;
using System;

namespace Application.ViewModels
{
    /// <summary>
    /// 用户视图模型
    /// </summary>
    public class UserViewModel : ViewModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 姓氏
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 名
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birth { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 查询全名
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return $"{LastName}/{FirstName}";
        }
    }
}
