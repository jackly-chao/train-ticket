﻿using Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels
{
    /// <summary>
    /// 视图模型(DTO)
    /// </summary>
    public abstract class ViewModel
    {
        /// <summary>
        /// ID
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }

        public bool IsOpen()
        {
            return Status == (int)StatusEnum.Open;
        }
    }
}
