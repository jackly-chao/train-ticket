﻿using Application.ViewModels;
using AutoMapper;
using Domain.Commands.Models;
using Domain.Models;

namespace Application.AutoMappers
{
    /// <summary>
    /// 自动映射文件
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region 领域模型->视图模型
            CreateMap<DefaultDomainModel, DefaultViewModel>()
              .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
              .ForMember(d => d.Name, o => o.MapFrom(s => s.Name))
              .ForMember(d => d.EnName, o => o.MapFrom(s => s.EnName))
              ;

            CreateMap<MenuDomainModel, MenuViewModel>();

            CreateMap<PermissionDomainModel, PermissionViewModel>();

            CreateMap<RolePermissionDomainModel, RolePermissionViewModel>();

            CreateMap<UserDomainModel, UserViewModel>();

            CreateMap<RoleDomainModel, RoleViewModel>();

            CreateMap<UserRoleDomainModel, UserRoleViewModel>();

            CreateMap<UserClaimDomainModel, UserClaimViewModel>();

            CreateMap<CatDomainModel, CatViewModel>();
            #endregion

            #region 视图模型->领域模型
            CreateMap<DefaultViewModel, DefaultDomainModel>()
              .ForPath(d => d.Id, o => o.MapFrom(s => s.Id))
              .ForPath(d => d.Name, o => o.MapFrom(s => s.Name))
              .ForPath(d => d.EnName, o => o.MapFrom(s => s.EnName))
              ;

            CreateMap<DefaultViewModel, DefaultAddCommandModel>().ConstructUsing(d => new DefaultAddCommandModel(d.Id, d.Name, d.EnName));

            CreateMap<DefaultViewModel, DefaultUpdateCommandModel>().ConstructUsing(d => new DefaultUpdateCommandModel(d.Id, d.Name, d.EnName));

            CreateMap<DefaultViewModel, DefaultDeleteCommandModel>().ConstructUsing(d => new DefaultDeleteCommandModel(d.Id));

            CreateMap<MenuViewModel, MenuDomainModel>();

            CreateMap<MenuViewModel, MenuAddCommandModel>().ConstructUsing(d => new MenuAddCommandModel(d.Id, d.Name, d.EnName, d.Status));

            CreateMap<MenuViewModel, MenuUpdateCommandModel>().ConstructUsing(d => new MenuUpdateCommandModel(d.Id, d.Name, d.EnName, d.Status));

            CreateMap<MenuViewModel, MenuDeleteCommandModel>().ConstructUsing(d => new MenuDeleteCommandModel(d.Id));

            CreateMap<PermissionViewModel, PermissionDomainModel>();

            CreateMap<PermissionViewModel, PermissionAddCommandModel>().ConstructUsing(d => new PermissionAddCommandModel(d.Id, d.Name, d.EnName, d.Route, d.Method, d.MenuId, d.Status));

            CreateMap<PermissionViewModel, PermissionUpdateCommandModel>().ConstructUsing(d => new PermissionUpdateCommandModel(d.Id, d.Name, d.EnName, d.Route, d.Method, d.MenuId, d.Status));

            CreateMap<PermissionViewModel, PermissionDeleteCommandModel>().ConstructUsing(d => new PermissionDeleteCommandModel(d.Id));

            CreateMap<RolePermissionViewModel, RolePermissionDomainModel>();

            CreateMap<RolePermissionViewModel, RolePermissionAddCommandModel>().ConstructUsing(d => new RolePermissionAddCommandModel(d.Id, d.RoleId, d.PermissionId, d.Status));

            CreateMap<RolePermissionViewModel, RolePermissionUpdateCommandModel>().ConstructUsing(d => new RolePermissionUpdateCommandModel(d.Id, d.RoleId, d.PermissionId, d.Status));

            CreateMap<RolePermissionViewModel, RolePermissionDeleteCommandModel>().ConstructUsing(d => new RolePermissionDeleteCommandModel(d.Id));

            CreateMap<UserViewModel, UserDomainModel>();

            CreateMap<RoleViewModel, RoleDomainModel>();

            CreateMap<UserRoleViewModel, UserRoleDomainModel>();

            CreateMap<UserClaimViewModel, UserClaimDomainModel>();

            CreateMap<CatViewModel, CatDomainModel>();

            CreateMap<CatViewModel, CatAddCommandModel>().ConstructUsing(d => new CatAddCommandModel(d.Id, d.Name, d.EnName, d.Status));

            CreateMap<CatViewModel, CatUpdateCommandModel>().ConstructUsing(d => new CatUpdateCommandModel(d.Id, d.Name, d.EnName, d.Status));

            CreateMap<CatViewModel, CatDeleteCommandModel>().ConstructUsing(d => new CatDeleteCommandModel(d.Id));
            #endregion
        }
    }
}
