﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 权限服务
    /// </summary>
    public class PermissionService : Service<PermissionViewModel, IDefaultRepository, PermissionDomainModel>, IPermissionService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IDefaultBus _bus;

        public PermissionService(IDefaultRepository repository, IMapper mapper, IDefaultBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public new async Task AddAsync(PermissionViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<PermissionAddCommandModel>(viewModel));
        }

        public new async Task UpdateAsync(PermissionViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<PermissionUpdateCommandModel>(viewModel));
        }

        public new async Task DeleteByIdAsync(object id)
        {
            await _bus.SendCommandAsync(new PermissionDeleteCommandModel((int)id));
        }
    }
}
