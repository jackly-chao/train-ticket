﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.IRepositories;
using Domain.Models;
using Extension.Attributes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 服务
    /// </summary>   
    /// <typeparam name="TViewModel">泛型视图模型</typeparam>
    /// <typeparam name="TIRepository">泛型仓储接口</typeparam>
    /// <typeparam name="TDomainModel">泛型领域模型</typeparam>
    public abstract class Service<TViewModel, TIRepository, TDomainModel> : IService<TViewModel> where TViewModel : ViewModel where TIRepository : IRepository where TDomainModel : DomainModel
    {
        /// <summary>
        /// 仓储接口
        /// </summary>
        protected readonly TIRepository _repository;

        /// <summary>
        /// 自动映射
        /// </summary>
        protected readonly IMapper _mapper;

        public Service(TIRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        #region 同步方法
        public List<TViewModel> GetList()
        {
            return _mapper.Map<List<TViewModel>>(_repository.GetList<TDomainModel>());
        }

        [GetCacheById]
        public TViewModel GetById(object id)
        {
            return _mapper.Map<TViewModel>(_repository.GetById<TDomainModel>(id));
        }

        public void Add(TViewModel viewModel)
        {
            _repository.Add(_mapper.Map<TDomainModel>(viewModel));
        }

        public void Update(TViewModel viewModel)
        {
            _repository.Update(_mapper.Map<TDomainModel>(viewModel));
        }

        public void DeleteById(object id)
        {
            _repository.DeleteById<TDomainModel>(id);
        }

        public int Save()
        {
           return _repository.Save();
        }
        #endregion

        #region 异步方法
        public async Task<List<TViewModel>> GetListAsync()
        {
            return _mapper.Map<List<TViewModel>>(await _repository.GetListAsync<TDomainModel>());
        }

        [GetCacheById]
        public async Task<TViewModel> GetByIdAsync(object id)
        {
            return _mapper.Map<TViewModel>(await _repository.GetByIdAsync<TDomainModel>(id));
        }

        public async Task AddAsync(TViewModel viewModel)
        {
            await _repository.AddAsync(_mapper.Map<TDomainModel>(viewModel));
        }

        public async Task UpdateAsync(TViewModel viewModel)
        {
            await _repository.UpdateAsync(_mapper.Map<TDomainModel>(viewModel));
        }

        public async Task DeleteByIdAsync(object id)
        {
            await _repository.DeleteByIdAsync<TDomainModel>(id);
        }

        public async Task<int> SaveAsync()
        {
            return await _repository.SaveAsync();
        }
        #endregion
    }
}
