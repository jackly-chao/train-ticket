﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Common.Enums;
using Domain.IRepositories;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 用户声明服务
    /// </summary>
    public class UserClaimService : Service<UserClaimViewModel, IIdentityRepository, UserClaimDomainModel>, IUserClaimService
    {
        public UserClaimService(IIdentityRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }

        public List<UserClaimViewModel> GetListByUserId(int userId)
        {
            return _mapper.Map<List<UserClaimViewModel>>(_repository.GetList<UserClaimDomainModel>(d => d.UserId == userId));
        }

        public async Task<List<UserClaimViewModel>> GetListByUserIdAsync(int userId)
        {
            return _mapper.Map<List<UserClaimViewModel>>(await _repository.GetListAsync<UserClaimDomainModel>(d => d.UserId == userId));
        }
    }
}
