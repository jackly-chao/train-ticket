﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 菜单服务
    /// </summary>
    public class MenuService : Service<MenuViewModel, IDefaultRepository, MenuDomainModel>, IMenuService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IDefaultBus _bus;

        public MenuService(IDefaultRepository repository, IMapper mapper, IDefaultBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public new async Task AddAsync(MenuViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<MenuAddCommandModel>(viewModel));
        }

        public new async Task UpdateAsync(MenuViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<MenuUpdateCommandModel>(viewModel));
        }

        public new async Task DeleteByIdAsync(object id)
        {
            await _bus.SendCommandAsync(new MenuDeleteCommandModel((int)id));
        }
    }
}

