﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.IRepositories;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 用户角色服务
    /// </summary>
    public class UserRoleService : Service<UserRoleViewModel, IIdentityRepository, UserRoleDomainModel>, IUserRoleService
    {
        public UserRoleService(IIdentityRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }

        public List<UserRoleViewModel> GetListByUserId(object userId)
        {
            return _mapper.Map<List<UserRoleViewModel>>(_repository.GetList<UserRoleDomainModel>(d => d.UserId == (int)userId));
        }

        public List<UserRoleViewModel> GetListByRoleId(object roleId)
        {
            return _mapper.Map<List<UserRoleViewModel>>(_repository.GetList<UserRoleDomainModel>(d => d.RoleId == (int)roleId));
        }

        public async Task<List<UserRoleViewModel>> GetListByUserIdAsync(object userId)
        {
            return _mapper.Map<List<UserRoleViewModel>>(await _repository.GetListAsync<UserRoleDomainModel>(d => d.UserId == (int)userId));
        }

        public async Task<List<UserRoleViewModel>> GetListByRoleIdAsync(object roleId)
        {
            return _mapper.Map<List<UserRoleViewModel>>(await _repository.GetListAsync<UserRoleDomainModel>(d => d.RoleId == (int)roleId));
        }
    }
}
