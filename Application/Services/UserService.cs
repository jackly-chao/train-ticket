﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Common.Enums;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 用户服务
    /// </summary>
    public class UserService : Service<UserViewModel, IIdentityRepository, UserDomainModel>, IUserService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IIdentityBus _bus;

        public UserService(IIdentityRepository repository, IMapper mapper, IIdentityBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public UserViewModel GetByUserName(string userName)
        {
            return _mapper.Map<UserViewModel>( _repository.Get<UserDomainModel>(d => d.UserName == userName && d.Status == (int)StatusEnum.Open));
        }

        public void Login(string userName, string password)
        {
            _bus.SendCommandAsync(new UserLoginCommandModel(userName, password));
        }

        public async Task<UserViewModel> GetByUserNameAsync(string userName)
        {
            return _mapper.Map<UserViewModel>(await _repository.GetAsync<UserDomainModel>(d => d.UserName == userName && d.Status == (int)StatusEnum.Open));
        }

        public async Task LoginAsync(string userName, string password)
        {
            await _bus.SendCommandAsync(new UserLoginCommandModel(userName, password));
        }
    }
}
