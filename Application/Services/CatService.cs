﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 猫咪服务
    /// </summary>
    public class CatService : Service<CatViewModel, IDefaultRepository, CatDomainModel>, ICatService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IDefaultBus _bus;

        public CatService(IDefaultRepository repository, IMapper mapper, IDefaultBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public new async Task AddAsync(CatViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<CatAddCommandModel>(viewModel));
        }

        public new async Task UpdateAsync(CatViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<CatUpdateCommandModel>(viewModel));
        }

        public new async Task DeleteByIdAsync(object id)
        {
            await _bus.SendCommandAsync(new CatDeleteCommandModel((int)id));
        }
    }
}
