﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 角色权限服务
    /// </summary>
    public class RolePermissionService : Service<RolePermissionViewModel, IDefaultRepository, RolePermissionDomainModel>, IRolePermissionService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IDefaultBus _bus;

        public RolePermissionService(IDefaultRepository repository, IMapper mapper, IDefaultBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public List<RolePermissionViewModel> GetListByRoleId(int roleId)
        {
            return _mapper.Map<List<RolePermissionViewModel>>(_repository.GetList<RolePermissionDomainModel>(d => d.RoleId == roleId));
        }

        public async Task<List<RolePermissionViewModel>> GetListByRoleIdAsync(int roleId)
        {
            return _mapper.Map<List<RolePermissionViewModel>>(await _repository.GetListAsync<RolePermissionDomainModel>(d => d.RoleId == roleId));
        }

        public new async Task AddAsync(RolePermissionViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<RolePermissionAddCommandModel>(viewModel));
        }

        public new async Task UpdateAsync(RolePermissionViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<RolePermissionUpdateCommandModel>(viewModel));
        }

        public new async Task DeleteByIdAsync(object id)
        {
            await _bus.SendCommandAsync(new RolePermissionDeleteCommandModel((int)id));
        }
    }
}
