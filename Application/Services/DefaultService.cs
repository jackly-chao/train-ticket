﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Buses.Interfaces;
using Domain.Commands.Models;
using Domain.IRepositories;
using Domain.Models;
using System.Threading.Tasks;

namespace Application.Services
{
    /// <summary>
    /// 默认服务
    /// </summary>
    public class DefaultService : Service<DefaultViewModel,IDefaultRepository, DefaultDomainModel>, IDefaultService
    {
        /// <summary>
        /// 总线(中介者)处理接口
        /// </summary>
        private readonly IDefaultBus _bus;

        public DefaultService(IDefaultRepository repository, IMapper mapper, IDefaultBus bus) : base(repository, mapper)
        {
            _bus = bus;
        }

        public new async Task AddAsync(DefaultViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<DefaultAddCommandModel>(viewModel));
        }

        public new async Task UpdateAsync(DefaultViewModel viewModel)
        {
            await _bus.SendCommandAsync(_mapper.Map<DefaultUpdateCommandModel>(viewModel));
        }

        public new async Task DeleteByIdAsync(object id)
        {
            await _bus.SendCommandAsync(new DefaultDeleteCommandModel((int)id));
        }
    }
}
