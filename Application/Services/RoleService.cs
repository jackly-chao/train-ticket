﻿using Application.IServices;
using Application.ViewModels;
using AutoMapper;
using Domain.IRepositories;
using Domain.Models;

namespace Application.Services
{
    /// <summary>
    /// 角色服务
    /// </summary>
    public class RoleService : Service<RoleViewModel, IIdentityRepository, RoleDomainModel>, IRoleService
    {
        public RoleService(IIdentityRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
