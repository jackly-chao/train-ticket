﻿using Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IServices
{
    /// <summary>
    /// 用户声明接口
    /// </summary>
    public interface IUserClaimService : IService<UserClaimViewModel>
    {
        /// <summary>
        /// 根据用户ID查询列表
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        List<UserClaimViewModel> GetListByUserId(int userId);

        /// <summary>
        /// 根据用户ID异步查询列表
        /// </summary>      
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        Task<List<UserClaimViewModel>> GetListByUserIdAsync(int userId);
    }
}
