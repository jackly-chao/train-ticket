﻿using Application.ViewModels;

namespace Application.IServices
{
    /// <summary>
    /// 默认服务接口
    /// </summary>
    public interface IDefaultService : IService<DefaultViewModel>
    {

    }
}
