﻿using Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IServices
{
    /// <summary>
    /// 角色权限服务接口
    /// </summary>
    public interface IRolePermissionService : IService<RolePermissionViewModel>
    {
        /// <summary>
        /// 根据角色ID查询列表
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        List<RolePermissionViewModel> GetListByRoleId(int roleId);

        /// <summary>
        /// 根据角色ID异步查询列表
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        Task<List<RolePermissionViewModel>> GetListByRoleIdAsync(int roleId);
    }
}
