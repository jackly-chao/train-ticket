﻿using Application.ViewModels;

namespace Application.IServices
{
    /// <summary>
    /// 角色服务接口
    /// </summary>
    public interface IRoleService : IService<RoleViewModel>
    {

    }
}
