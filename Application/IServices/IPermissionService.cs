﻿using Application.ViewModels;

namespace Application.IServices
{
    /// <summary>
    /// 权限服务接口
    /// </summary>
    public interface IPermissionService : IService<PermissionViewModel>
    {

    }
}
