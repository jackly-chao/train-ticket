﻿using Application.ViewModels;
using System.Threading.Tasks;

namespace Application.IServices
{
    /// <summary>
    /// 用户服务接口
    /// </summary>
    public interface IUserService : IService<UserViewModel>
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        void Login(string userName, string password);

        /// <summary>
        /// 根据用户名查询
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        UserViewModel GetByUserName(string userName);

        /// <summary>
        /// 异步登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        Task LoginAsync(string userName, string password);

        /// <summary>
        /// 根据用户名异步查询
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        Task<UserViewModel> GetByUserNameAsync(string userName);
    }
}
