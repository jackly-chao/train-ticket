﻿using Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IServices
{
    /// <summary>
    /// 服务接口
    /// </summary>
    public interface IService<TViewModel> where TViewModel : ViewModel
    {
        #region 同步方法
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <returns></returns>
        List<TViewModel> GetList();

        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        TViewModel GetById(object id);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="viewModel">视图模型</param>
        void Add(TViewModel viewModel);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="viewModel">视图模型</param>
        void Update(TViewModel viewModel);

        /// <summary>
        /// 根据ID删除
        /// </summary>
        /// <param name="id">ID</param>
        void DeleteById(object id);

        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        int Save();
        #endregion

        #region 异步方法
        /// <summary>
        /// 异步查询列表
        /// </summary>
        /// <returns></returns>
        Task<List<TViewModel>> GetListAsync();

        /// <summary>
        /// 根据ID异步查询
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task<TViewModel> GetByIdAsync(object id);

        /// <summary>
        /// 异步新增
        /// </summary>
        /// <param name="viewModel">视图模型</param>
        /// <returns></returns>
        Task AddAsync(TViewModel viewModel);

        /// <summary>
        /// 异步修改
        /// </summary>
        /// <param name="viewModel">视图模型</param>
        Task UpdateAsync(TViewModel viewModel);

        /// <summary>
        /// 根据ID异步删除
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        Task DeleteByIdAsync(object id);

        /// <summary>
        /// 异步保存
        /// </summary>
        /// <returns></returns>
        Task<int> SaveAsync();
        #endregion
    }
}
