﻿using Application.ViewModels;

namespace Application.IServices
{
    /// <summary>
    /// 猫咪服务接口
    /// </summary>
    public interface ICatService : IService<CatViewModel>
    {

    }
}
