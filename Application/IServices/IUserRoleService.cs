﻿using Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IServices
{
    /// <summary>
    /// 用户角色服务接口
    /// </summary>
    public interface IUserRoleService : IService<UserRoleViewModel>
    {
        /// <summary>
        /// 根据用户ID查询列表
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        List<UserRoleViewModel> GetListByUserId(object userId);

        /// <summary>
        /// 根据角色ID查询列表
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        List<UserRoleViewModel> GetListByRoleId(object roleId);

        /// <summary>
        /// 根据用户ID异步查询列表
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        Task<List<UserRoleViewModel>> GetListByUserIdAsync(object userId);

        /// <summary>
        /// 根据角色ID异步查询列表
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        Task<List<UserRoleViewModel>> GetListByRoleIdAsync(object roleId);
    }
}
