﻿using Application.ViewModels;

namespace Application.IServices
{
    /// <summary>
    /// 菜单服务接口
    /// </summary>
    public interface IMenuService : IService<MenuViewModel>
    {

    }
}
