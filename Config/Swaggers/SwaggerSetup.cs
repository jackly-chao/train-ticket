﻿using Common.Enums;
using Config.Auths;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Config.Swaggers
{
    //参考：https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-5.0&tabs=visual-studio
    public static class SwaggerSetup
    {
        /// <summary>
        /// 配置OpenAPI服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureSwaggerService(this IServiceCollection services, IConfiguration configuration, string nameSpace)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSwaggerGen(c =>
            {
                typeof(VersionEnum).GetEnumNames().ToList().ForEach(version =>
                {
                    c.SwaggerDoc(version, new OpenApiInfo
                    {
                        Title = configuration["APIName"] ?? nameSpace,
                        Version = version,
                        Description = $"{configuration["APIDescription"] ?? nameSpace} {version}",
                        Contact = new OpenApiContact
                        {
                            Name = configuration["ContactName"] ?? "None",
                            Email = configuration["ContactEmail"] ?? "None"
                        }
                    });
                });

                // 开启加权小锁
                c.OperationFilter<AddResponseHeadersFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();

                // 在header中添加token，传递到后台
                c.OperationFilter<SecurityRequirementsOperationFilter>();

                #region Ids4
                if (Permission.IsUseIds4)
                {
                    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.OAuth2,
                        Flows = new OpenApiOAuthFlows
                        {
                            Implicit = new OpenApiOAuthFlow
                            {
                                AuthorizationUrl = new Uri($"{configuration["AuthorizationUrl"]}/connect/authorize"),
                                TokenUrl = new Uri($"{configuration["AuthorizationUrl"]}/connect/token"),
                                Scopes = new Dictionary<string, string>
                                {
                                    { configuration["APIScope"],configuration["APIScopeDescription"] }
                                }
                            }
                        }
                    });
                }
                #endregion

                #region JWT
                else
                {
                    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.ApiKey,
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Description = "JWT授权（数据将在请求头中进行传输）直接在下框中输入Bearer {token}（注意两者之间是一个空格）"
                    });
                } 
                #endregion

                //为 Swagger JSON and UI设置xml文档注释路径
                var viewModelXmlPath = Path.Combine(AppContext.BaseDirectory, "Application.xml");
                c.IncludeXmlComments(viewModelXmlPath, true);

                var controllerXmlPath = Path.Combine(AppContext.BaseDirectory, $"{nameSpace}.xml");
                c.IncludeXmlComments(controllerXmlPath, true);

                var commonXmlPath = Path.Combine(AppContext.BaseDirectory, "Common.xml");
                c.IncludeXmlComments(commonXmlPath, true);
            });
        }

        /// <summary>
        /// 配置OpenAPI
        /// </summary>
        /// <param name="app">应用</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureSwagger(this IApplicationBuilder app, IConfiguration configuration, string nameSpace)
        {
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    typeof(VersionEnum).GetEnumNames().OrderByDescending(e => e).ToList().ForEach(version =>
                    {
                        c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{configuration["APIName"] ?? nameSpace} {version}");
                        c.OAuthClientId($"{configuration["ClientId"]}");
                        c.OAuthAppName($"{configuration["ClientName"]}");
                        c.RoutePrefix = string.Empty;//在应用的根 (http://localhost:<port>/) 处提供 Swagger UI
                    });
                });
        }
    }
}
