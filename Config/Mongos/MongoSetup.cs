﻿using Infrastruct.MongoSettings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Config.Mongos
{
    public static class MongoSetup
    {
        /// <summary>
        /// 配置Mongo服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureMongoService(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSingleton(new DefaultMongoSetting(configuration.GetConnectionString("DefaultMongo"), configuration.GetConnectionString("DefaultMongoDbName")));
        }
    }
}
