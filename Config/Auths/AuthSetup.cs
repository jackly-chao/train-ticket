﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Config.Auths
{
    public static class AuthSetup
    {
        /// <summary>
        /// 配置认证授权服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureAuthService(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region 认证
            #region Ids4
            if (Permission.IsUseIds4)
            {
                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = nameof(AuthenticationHandler);
                    options.DefaultForbidScheme = nameof(AuthenticationHandler);

                }).AddJwtBearer(options =>
                {
                    options.Authority = configuration["AuthorizationUrl"];
                    options.RequireHttpsMetadata = true;
                    options.Audience = configuration["Audience"];

                }).AddScheme<AuthenticationSchemeOptions, AuthenticationHandler>(nameof(AuthenticationHandler), o => { });
            }
            #endregion

            #region JWT
            else
            {
                var issuer = configuration["Issuer"];
                var audience = configuration["Audience"];
                var issuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["Password"]));

                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = issuer,
                    ValidateAudience = true,
                    ValidAudience = audience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = issuerSigningKey,
                    RequireExpirationTime = true,
                    ClockSkew = TimeSpan.FromSeconds(30),
                };

                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = nameof(AuthenticationHandler);
                    options.DefaultForbidScheme = nameof(AuthenticationHandler);

                }).AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = tokenValidationParameters;
                    options.Events = new JwtBearerEvents
                    {
                        OnChallenge = context =>
                        {
                            context.Response.Headers.Add("Token-Error", context.ErrorDescription);
                            return Task.CompletedTask;
                        },
                        OnAuthenticationFailed = context =>
                        {
                            var token = context.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                            var jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
                            if (jwtToken.Issuer != issuer)
                            {
                                context.Response.Headers.Add("Token-Error-Iss", "Issuer is wrong!");
                            }
                            if (jwtToken.Audiences.FirstOrDefault() != audience)
                            {
                                context.Response.Headers.Add("Token-Error-Aud", "Audience is wrong!");
                            }
                            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                            {
                                context.Response.Headers.Add("Token-Expired", "true");
                            }
                            return Task.CompletedTask;
                        }
                    };

                }).AddScheme<AuthenticationSchemeOptions, AuthenticationHandler>(nameof(AuthenticationHandler), o => { });
            }
            #endregion
            #endregion

            #region 授权
            services.AddAuthorization(options =>
            {
                options.AddPolicy("GetById", policy =>
                {
                    policy.RequireScope("GetById");
                });
            });

            var requirement = new PermissionRequirement 
            {
                Permissions = new List<PermissionItem>(),
                ClaimType = ClaimTypes.Role,
                Issuer = configuration["Issuer"],
                Audience = configuration["Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["Password"])), SecurityAlgorithms.HmacSha256),
                Expiration = TimeSpan.FromMinutes(30)
            };

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Permission.Name, policy => policy.Requirements.Add(requirement));
            });

            services.AddSingleton(requirement);

            services.AddScoped<IAuthorizationHandler, AuthorizationHandler>(); 
            #endregion
        }
    }
}
