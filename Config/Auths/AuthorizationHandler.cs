﻿using Application.IServices;
using Application.ViewModels;
using Common.Enums;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Config.Auths
{
    /// <summary>
    /// 授权处理程序
    /// </summary>
    public class AuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        /// <summary>
        /// http上下文
        /// </summary>
        private readonly IHttpContextAccessor _accessor;

        /// <summary>
        /// 认证框架
        /// </summary>
        private readonly IAuthenticationSchemeProvider _schemes;

        /// <summary>
        /// 用户服务
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        /// 用户角色服务
        /// </summary>
        private readonly IUserRoleService _userRoleService;

        /// <summary>
        /// 权限服务
        /// </summary>
        private readonly IPermissionService _permissionService;

        /// <summary>
        /// 角色权限服务
        /// </summary>
        private readonly IRolePermissionService _rolePermissionService;

        /// <summary>
        /// 角色服务
        /// </summary>
        private readonly IRoleService _roleService;

        public AuthorizationHandler(IHttpContextAccessor accessor, IAuthenticationSchemeProvider schemes, IUserService userService, IUserRoleService userRoleService, IRoleService roleService, IRolePermissionService rolePermissionService, IPermissionService permissionService)
        {
            _accessor = accessor;
            _schemes = schemes;
            _userService = userService;
            _userRoleService = userRoleService;
            _permissionService = permissionService;
            _rolePermissionService = rolePermissionService;
            _roleService = roleService;
        }

        // 重写异步处理程序
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            try
            {
                var httpContext = _accessor.HttpContext;
                if (httpContext == null) throw new Exception();

                // 整体结构类似认证中间件UseAuthentication的逻辑，具体查看开源地址
                // https://github.com/dotnet/aspnetcore/blob/master/src/Security/Authentication/Core/src/AuthenticationMiddleware.cs
                httpContext.Features.Set<IAuthenticationFeature>(new AuthenticationFeature
                {
                    OriginalPath = httpContext.Request.Path,
                    OriginalPathBase = httpContext.Request.PathBase
                });

                // 主要作用是: 判断当前是否需要进行远程验证，如果是就进行远程验证
                var handlers = httpContext.RequestServices.GetRequiredService<IAuthenticationHandlerProvider>();
                foreach (var scheme in await _schemes.GetRequestHandlerSchemesAsync())
                {
                    if (await handlers.GetHandlerAsync(httpContext, scheme.Name) is IAuthenticationRequestHandler handler && await handler.HandleRequestAsync()) throw new Exception();
                }

                //判断请求是否拥有凭据，即有没有登录，result?.Principal为空表示登录失败
                var defaultAuthenticate = await _schemes.GetDefaultAuthenticateSchemeAsync();
                if (defaultAuthenticate == null) throw new Exception();
                var result = await httpContext.AuthenticateAsync(defaultAuthenticate.Name);
                if (result?.Principal == null) throw new Exception();

                httpContext.User = result.Principal;
                // 查询系统中所有的角色和菜单的关系集合
                if (requirement.Permissions == null || !requirement.Permissions.Any())
                {
                    var userId = httpContext.User.Claims.Where(d => d.Type == "user_id").Select(d => d.Value).FirstOrDefault();
                    var user = _userService.GetById(int.Parse(userId ?? "0"));
                    if (user == null || !user.IsOpen()) throw new Exception();
                    var userRoles = _userRoleService.GetListByUserId(user.Id);
                    if (userRoles == null || !userRoles.Any()) throw new Exception();
                    var roleIds = new List<int>();
                    foreach (var userRole in userRoles)
                    {
                        var role = _roleService.GetById(userRole.RoleId);
                        if (role == null || !role.IsOpen()) continue;
                        roleIds.Add(role.Id);
                    }
                    if (roleIds == null || !roleIds.Any()) throw new Exception();
                    var rolePermissions = new List<RolePermissionViewModel>();
                    foreach (var roleId in roleIds)
                    {
                        rolePermissions.AddRange(_rolePermissionService.GetListByRoleId(roleId)?.Where(d => d.Status == (int)StatusEnum.Open));
                    }
                    if (rolePermissions == null || !rolePermissions.Any()) throw new Exception();
                    var permissions = new List<PermissionViewModel>();
                    foreach (var rolePermission in rolePermissions)
                    {
                        if (permissions.Any(d => d.Id == rolePermission.PermissionId)) continue;
                        permissions.Add(await _permissionService.GetByIdAsync(rolePermission.PermissionId));
                    }
                    if (permissions == null || !permissions.Any()) throw new Exception();
                    requirement.Permissions = permissions.Select(d => new PermissionItem { Url = d.Route.ToLower(), Method = Enum.GetName(typeof(MethodEnum), d.Method).ToLower() }).ToList();
                }
                var questUrl = httpContext.Request.Path.Value.ToLower();
                var questMethod = httpContext.Request.Method.ToLower();
                foreach (var version in typeof(VersionEnum).GetEnumNames())
                {
                    questUrl = questUrl.Replace($"/{version}", "");
                }
                if (!requirement.Permissions.Any(d => (d.Url == questUrl || Regex.IsMatch(questUrl, d.Url + @"(/[^\s\n]*)")) && d.Method == questMethod)) throw new Exception();
                //正则表达式Regex.IsMatch(questUrl, d.Url + @"(/[^\s\n]*)")是为了让/api/menu和/api/menu/2能够匹配
                context.Succeed(requirement);
            }
            catch (Exception ex)
            {
                context.Fail();
            }
        }
    }

    public class PermissionRequirement : IAuthorizationRequirement
    {
        ///// <summary>
        ///// 构造
        ///// </summary>
        ///// <param name="deniedAction">拒约请求的url</param>
        ///// <param name="permissions">权限集合</param>
        ///// <param name="claimType">声明类型</param>
        ///// <param name="issuer">发行人</param>
        ///// <param name="audience">订阅人</param>
        ///// <param name="signingCredentials">签名验证实体</param>
        ///// <param name="expiration">过期时间</param>
        //public PermissionRequirement(string deniedAction, List<PermissionItem> permissions, string claimType, string issuer, string audience, SigningCredentials signingCredentials, TimeSpan expiration)
        //{
        //    ClaimType = claimType;
        //    DeniedAction = deniedAction;
        //    Permissions = permissions;
        //    Issuer = issuer;
        //    Audience = audience;
        //    Expiration = expiration;
        //    SigningCredentials = signingCredentials;
        //}

        /// <summary>
        /// 用户权限集合，一个订单包含了很多详情，
        /// 同理，一个网站的认证发行中，也有很多权限详情(这里是Role和URL的关系)
        /// </summary>
        public List<PermissionItem> Permissions { get; set; }

        /// <summary>
        /// 认证授权类型
        /// </summary>
        public string ClaimType { internal get; set; } = ClaimTypes.Role;

        /// <summary>
        /// 发行人
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 订阅人
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 签名验证
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public TimeSpan Expiration { get; set; }

        /// <summary>
        /// 无权限action
        /// </summary>
        public string DeniedAction { get; set; }

        /// <summary>
        /// 请求路径
        /// </summary>
        public string LoginPath { get; set; } = "/Api/Login";
    }

    /// <summary>
    /// 用户或角色或其他凭据实体,就像是订单详情一样
    /// 之前的名字是 Permission
    /// </summary>
    public class PermissionItem
    {
        /// <summary>
        /// 用户或角色或其他凭据名称
        /// </summary>
        public virtual string Role { get; set; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public virtual string Url { get; set; }

        /// <summary>
        /// 请求方法
        /// </summary>
        public virtual string Method { get; set; }
    }

    public static class Permission
    {
        public const string Name = "Permission";

        public static bool IsUseIds4 = true;
    }
}
