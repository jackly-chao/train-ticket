﻿using Application.AutoMappers;
using Application.Notifications;
using Application.Services;
using Autofac;
using Autofac.Extras.DynamicProxy;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using Domain.Buses.Handlers;
using Domain.Commands.Handlers;
using Domain.Events.Handlers;
using Domain.Notifications.Handlers;
using Extension.AOPs;
using Infrastruct.EventSourceRepositories;
using Infrastruct.Mongos;
using Infrastruct.RabbitMQs;
using Infrastruct.Redises;
using Infrastruct.Repositories;
using Infrastruct.UnitOfWorks;
using MediatR;
using System.Linq;

namespace Config.Autofacs
{
    // 参考:https://github.com/autofac/Examples/blob/master/src/AspNetCore3Example/AutofacModule.cs
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region 服务
            builder.RegisterAssemblyTypes(typeof(DefaultService).Assembly)
                .Where(d => d.Name.Contains("Service"))
                .AsImplementedInterfaces()
                .InstancePerDependency()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(ServiceAOP));//ServiceAOP
            #endregion

            #region 通知
            builder.RegisterAssemblyTypes(typeof(DefaultNotification).Assembly)
             .Where(d => d.Name.Contains("Notification"))
             .AsImplementedInterfaces()
             .InstancePerLifetimeScope();
            #endregion

            #region 自动映射
            builder.RegisterAutoMapper(typeof(AutoMapperProfile).Assembly);
            #endregion

            #region 命令处理程序
            //https://github.com/bernos/MediatR.Extension  和  https://stackoverflow.com/questions/42374101/mediatr-autofac-handler-registration-in-container

            builder.RegisterAssemblyTypes(typeof(DefaultCommandHandler).Assembly).AsClosedTypesOf(typeof(IRequestHandler<,>));

            //builder.RegisterType<DefaultCommandHandler>().As<IRequestHandler<DefaultAddCommandModel, bool>>().InstancePerDependency();
            //builder.RegisterType<DefaultCommandHandler>().As<IRequestHandler<DefaultUpdateCommandModel, bool>>().InstancePerDependency();
            //builder.RegisterType<DefaultCommandHandler>().As<IRequestHandler<DefaultDeleteCommandModel, bool>>().InstancePerDependency();

            //builder.RegisterType<CatCommandHandler>().As<IRequestHandler<CatAddCommandModel, bool>>().InstancePerDependency();
            //builder.RegisterType<CatCommandHandler>().As<IRequestHandler<CatUpdateCommandModel, bool>>().InstancePerDependency();
            //builder.RegisterType<CatCommandHandler>().As<IRequestHandler<CatDeleteCommandModel, bool>>().InstancePerDependency();
            #endregion

            #region 事件处理程序
            builder.RegisterAssemblyTypes(typeof(DefaultEventHandler).Assembly).AsClosedTypesOf(typeof(INotificationHandler<>));

            //builder.RegisterType<DefaultEventHandler>().As<INotificationHandler<DefaultAddEventModel>>().InstancePerDependency();
            //builder.RegisterType<DefaultEventHandler>().As<INotificationHandler<DefaultUpdateEventModel>>().InstancePerDependency();
            //builder.RegisterType<DefaultEventHandler>().As<INotificationHandler<DefaultDeleteEventModel>>().InstancePerDependency();

            //builder.RegisterType<CatEventHandler>().As<INotificationHandler<CatAddEventModel>>().InstancePerDependency();
            //builder.RegisterType<CatEventHandler>().As<INotificationHandler<CatUpdateEventModel>>().InstancePerDependency();
            //builder.RegisterType<CatEventHandler>().As<INotificationHandler<CatDeleteEventModel>>().InstancePerDependency();
            #endregion

            #region 通知处理程序
            builder.RegisterAssemblyTypes(typeof(DefaultNotificationHandler).Assembly).AsClosedTypesOf(typeof(INotificationHandler<>));

            //builder.RegisterAssemblyTypes(typeof(DefaultNotificationHandler).Assembly)
            // .Where(d => d.Name.Contains("Notification"))
            // .AsSelf()
            // .InstancePerLifetimeScope();
            #endregion

            #region 总线处理程序
            builder.RegisterAssemblyTypes(typeof(DefaultBus).Assembly)
                .Where(d => d.Name.Contains("Bus"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            #endregion

            #region 工作单元
            builder.RegisterAssemblyTypes(typeof(DefaultUnitOfWork).Assembly)
                .Where(d => d.Name.Contains("UnitOfWork"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            #endregion

            #region 仓储
            builder.RegisterAssemblyTypes(typeof(DefaultRepository).Assembly)
                .Where(d => d.Name.Contains("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(RepositoryAOP));//RepositoryAOP
            #endregion

            #region 事件溯源仓储
            builder.RegisterAssemblyTypes(typeof(DefaultEventSourceRepository).Assembly)
                .Where(d => d.Name.Contains("EventSourceRepository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            #endregion

            #region Redis
            builder.RegisterAssemblyTypes(typeof(DefaultRedis).Assembly)
                .Where(d => d.Name.Contains("Redis"))
                .AsImplementedInterfaces()
                .SingleInstance();//单例模式
            #endregion

            #region Mongo
            builder.RegisterAssemblyTypes(typeof(DefaultMongo).Assembly)
               .Where(d => d.Name.Contains("Mongo"))
               .AsImplementedInterfaces()
               .SingleInstance();//单例模式
            #endregion

            #region RabbitMQ
            builder.RegisterAssemblyTypes(typeof(DefaultRabbitMQ).Assembly)
              .Where(d => d.Name.Contains("RabbitMQ"))
              .AsImplementedInterfaces()
              .InstancePerLifetimeScope();
            #endregion

            #region AOP
            builder.RegisterAssemblyTypes(typeof(ServiceAOP).Assembly)
              .Where(d => d.Name.Contains("AOP"))
              .AsSelf();
            #endregion
        }
    }
}
