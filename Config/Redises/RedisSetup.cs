﻿using Infrastruct.RedisClients;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Config.Redises
{
    public static class RedisSetup
    {
        /// <summary>
        /// 配置Redis服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureRedisService(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            var defaultRedis = new DefaultRedisClient(configuration.GetConnectionString("DefaultRedis"));
            RedisHelper.Initialization(defaultRedis);
            services.AddSingleton(defaultRedis);
        }
    }
}
