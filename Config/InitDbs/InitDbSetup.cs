﻿using Infrastruct.DbContexts;
using Infrastruct.EventSourceDbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Config.InitDbs
{
    public static class InitDbSetup
    {
        /// <summary>
        /// 初始化数据库
        /// 
        /// 前提是在程序包管理器控制台（Package Manager Console）选择Infrastruct项目先做迁移
        /// 
        /// dotnet add package Microsoft.EntityFrameworkCore.Design
        /// 
        /// dotnet ef migrations add InitDefaultDb -c DefaultDbContext -o Migrations/DefaultDb
        /// 
        /// dotnet ef migrations add InitDefaultEventSourceDb -c DefaultEventSourceDbContext -o Migrations/DefaultEventSourceDb
        /// 
        /// dotnet ef migrations add InitIdentityEventSourceDb -c IdentityEventSourceDbContext -o Migrations/IdentityEventSourceDb
        /// 
        /// 如果不调用这个方法，需要手动在程序包管理器控制台（Package Manager Console）完成迁移
        /// 
        /// dotnet ef database update -c DefaultDbContext
        /// 
        /// dotnet ef database update -c DefaultEventSourceDbContext
        /// 
        /// dotnet ef database update -c IdentityEventSourceDbContext
        /// </summary>
        /// <param name="app">应用程序</param>
        public static void InitDb(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DefaultDbContext>();
                context.Database.Migrate();
                //if (!context.Cats.Any())
                //{
                //    context.Cats.Add(new Domain.Models.CatDomainModel(0, "测试猫", "TestCat"));
                //    context.SaveChanges();
                //}
            }

            serviceScope.ServiceProvider.GetRequiredService<DefaultEventSourceDbContext>().Database.Migrate();

            serviceScope.ServiceProvider.GetRequiredService<IdentityEventSourceDbContext>().Database.Migrate();
        }
    }
}
