﻿using Infrastruct.RabbitMQSettings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Config.RabbitMQs
{
    public static class RabbitMQSetup
    {
        /// <summary>
        /// 配置RabbitMQ服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureRabbitMQService(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSingleton(new DefaultRabbitMQSetting(configuration.GetConnectionString("DefaultRabbitMQHost"), configuration.GetConnectionString("DefaultRabbitMQVirtualHost"), configuration.GetConnectionString("DefaultRabbitMQUserName"), configuration.GetConnectionString("DefaultRabbitMQPassword"), configuration.GetConnectionString("DefaultRabbitMQExchangeName"), configuration.GetConnectionString("DefaultRabbitMQRoutingKey"), configuration.GetConnectionString("DefaultRabbitMQQueueName")));
        }
    }
}
