﻿using Infrastruct.DbContexts;
using Infrastruct.EventSourceDbContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace Config.DbContexts
{
    public static class DbContextSetup
    {
        /// <summary>
        /// 配置数据库上下文服务
        /// </summary>
        /// <param name="services">服务</param>
        /// <param name="configuration">配置</param>
        public static void ConfigureDbContextService(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            //数据库上下文
            services.AddDbContext<DefaultDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultDb"),
                sql => sql.MigrationsAssembly(typeof(DefaultDbContext).GetTypeInfo().Assembly.GetName().Name)));//, ServiceLifetime.Transient

            //事件溯源数据库上下文
            services.AddDbContext<DefaultEventSourceDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultEventSourceDb"),
                sql => sql.MigrationsAssembly(typeof(DefaultEventSourceDbContext).GetTypeInfo().Assembly.GetName().Name))) ;//, ServiceLifetime.Transient

            //认证数据库上下文
            services.AddDbContext<IdentityDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("IdentityDb"),
                sql => sql.MigrationsAssembly(typeof(IdentityDbContext).GetTypeInfo().Assembly.GetName().Name)));//, ServiceLifetime.Transient

            //认证溯源数据库上下文
            services.AddDbContext<IdentityEventSourceDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("IdentityEventSourceDb"),
                sql => sql.MigrationsAssembly(typeof(IdentityEventSourceDbContext).GetTypeInfo().Assembly.GetName().Name)));//, ServiceLifetime.Transient
        }
    }
}
