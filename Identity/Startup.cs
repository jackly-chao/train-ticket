using Identity.DbContext;
using Identity.Model;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;

namespace Identity
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //添加控制器的服务
            services.AddControllersWithViews();

            var connectionString = Configuration.GetConnectionString("IdentityDb");
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var builder = services.AddIdentityServer()
                //AddConfigurationStore()需要添加IdentityServer4.EntityFramework包
                .AddConfigurationStore(options =>
                {
                    //UseSqlServer()需要添加Microsoft.EntityFrameworkCore.SqlServer包
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                //AddAspNetIdentity需要添加IdentityServer4.AspNetIdentity包
                .AddAspNetIdentity<ApplicationUser>();

            if (Environment.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                builder.AddSigningCredential(new X509Certificate2(Path.Combine(basePath, Configuration["Certificates:Path"]),Configuration["Certificates:Password"]));//生产环境应该考虑使用证书
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                //初始化数据库
                InitializeDatabase(app);
            }

            //启用静态文件服务
            app.UseStaticFiles();

            app.UseRouting();

            //添加认证服务（UseIdentityServer()需要添加IdentityServer4包）
            app.UseIdentityServer();

            //添加授权中间件
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }

        /// <summary>
        /// 初始化数据库
        /// 参考：https://github.com/IdentityServer/IdentityServer4/blob/main/samples/Quickstarts/5_EntityFramework/src/IdentityServer/Startup.cs
        /// 
        /// 前提是在程序包管理器控制台（Package Manager Console）先做迁移
        /// dotnet add package Microsoft.EntityFrameworkCore.Design
        /// 
        /// dotnet ef migrations add InitialIdentityServerPersistedGrantDbMigration -c PersistedGrantDbContext -o Migrations/PersistedGrantDb
        /// 
        /// dotnet ef migrations add InitialIdentityServerConfigurationDbMigration -c ConfigurationDbContext -o Migrations/ConfigurationDb
        /// 
        /// dotnet ef migrations add InitialIdentityServerApplicationDbMigration  -c ApplicationDbContext  -o Migrations/ApplicationDb
        /// 
        /// 如果不调用这个方法，需要手动在程序包管理器控制台（Package Manager Console）完成迁移
        /// 
        /// update-database -Context PersistedGrantDbContext
        /// 
        /// update-database -Context ConfigurationDbContext
        /// 
        /// update-database -Context ApplicationDbContext
        /// </summary>
        /// <param name="app"></param>
        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                //初始化数据库结构
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                //初始化配置
                {
                    var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                    context.Database.Migrate();
                    if (!context.Clients.Any())
                    {
                        foreach (var client in Config.GetClients())
                        {
                            context.Clients.Add(client.ToEntity());
                        }
                        context.SaveChanges();
                    }

                    if (!context.IdentityResources.Any())
                    {
                        foreach (var resource in Config.GetIdentityResources())
                        {
                            context.IdentityResources.Add(resource.ToEntity());
                        }
                        context.SaveChanges();
                    }

                    if (!context.ApiScopes.Any())
                    {
                        foreach (var resource in Config.GetApiScopes().ToList())
                        {
                            context.ApiScopes.Add(resource.ToEntity());
                        }
                        context.SaveChanges();
                    }

                    if (!context.ApiResources.Any())
                    {
                        foreach (var resource in Config.GetApiResources().ToList())
                        {
                            context.ApiResources.Add(resource.ToEntity());
                        }
                        context.SaveChanges();
                    }
                }

                //初始化用户,参考：https://github.com/IdentityServer/IdentityServer4/blob/2b5d9b634aa5195e3f7867129836691c5b68bd7d/samples/Quickstarts/6_AspNetIdentity/src/IdentityServerAspNetIdentity/SeedData.cs
                {
                    var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();
                    var userMgr = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    if (!userMgr.Users.Any())
                    {
                        foreach (var user in Config.GetUsers().ToList())
                        {
                            var newUser = new ApplicationUser
                            {
                                UserName = user.Username,
                            };

                            var result = userMgr.CreateAsync(newUser, user.Password).Result;
                            if (result.Succeeded)
                            {
                                result = userMgr.AddClaimsAsync(newUser, new Claim[]{
                                    new Claim(JwtClaimTypes.Name, newUser.UserName),
                                    new Claim(JwtClaimTypes.GivenName, newUser.UserName),
                                    new Claim(JwtClaimTypes.FamilyName, newUser.UserName),
                                    new Claim(JwtClaimTypes.WebSite, "http://alice.com"),
                                }).Result;
                            }
                        }
                    }
                }
            }
        }
    }
}
