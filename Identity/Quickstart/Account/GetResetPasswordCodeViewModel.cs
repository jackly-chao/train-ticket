﻿using System.ComponentModel.DataAnnotations;

namespace Identity.Quickstart.Account
{
    /// <summary>
    /// 获取重置密码验证码视图模型
    /// </summary>
    public class GetResetPasswordCodeViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "邮箱")]
        public string Email { get; set; }
    }
}
