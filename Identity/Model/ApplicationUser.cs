﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Identity.Model
{
    /// <summary>
    /// 应用程序用户
    /// </summary>
    public class ApplicationUser : IdentityUser<int>
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 姓氏
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 名
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; } = (int)SexEnum.Male;

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birth { get; set; } = DateTime.Now;

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; } = (int)StatusEnum.Close;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 获取全名
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return $"{LastName}{FirstName}";
        }
    }

    /// <summary>
    /// 状态枚举
    /// </summary>
    public enum StatusEnum
    {
        /// <summary>
        /// 删除
        /// </summary>
        Delete = -1,

        /// <summary>
        /// 关闭
        /// </summary>
        Close = 0,

        /// <summary>
        /// 开启
        /// </summary>
        Open = 1
    }

    public enum SexEnum
    {
        /// <summary>
        /// 男性
        /// </summary>
        Male = 0,

        /// <summary>
        /// 女性
        /// </summary>
        Female = 1,
    }
}
