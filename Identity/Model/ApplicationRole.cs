﻿using Microsoft.AspNetCore.Identity;

namespace Identity.Model
{
    /// <summary>
    /// 应用程序角色
    /// </summary>
    public class ApplicationRole : IdentityRole<int>
    {

    }
}
