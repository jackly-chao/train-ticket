﻿using Common.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System;

namespace Extension.Attributes
{
    /// <summary>
    /// API版本路由标签，必须继承IApiDescriptionGroupNameProvider，并赋值GroupName，不然API无法分版本
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class APIVersionAttribute : RouteAttribute, IApiDescriptionGroupNameProvider
    {
        public string GroupName { get; set; }

        public APIVersionAttribute(VersionEnum version) : base($"/api/{version}/[controller]")
        {
            GroupName = version.ToString();
        }

        public APIVersionAttribute(VersionEnum version, string action = "[action]") : base($"/api/{version}/[controller]/{action}")
        {
            GroupName = version.ToString();
        }
    }
}
