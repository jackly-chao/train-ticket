﻿using System;

namespace Extension.Attributes
{
    /// <summary>
    /// 根据ID查询缓存便签，只能在方法上打标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Interface, Inherited = true)]
    public class GetCacheByIdAttribute : Attribute
    {
        /// <summary>
        /// 缓存查询时间（单位：秒）
        /// </summary>
        public int ExpireSeconds { get; set; } = 60;
    }
}
