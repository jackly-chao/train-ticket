﻿using Castle.DynamicProxy;
using Domain.IRedises;
using Extension.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Extension.AOPs
{
    /// <summary>
    /// Repository的切面编程
    /// </summary>
    public class RepositoryAOP : IInterceptor
    {
        /// <summary>
        /// Redis
        /// </summary>
        private readonly IDefaultRedis _redis;

        public RepositoryAOP(IDefaultRedis redis)
        {
            _redis = redis;
        }

        public void Intercept(IInvocation invocation)
        {
            var method = invocation.MethodInvocationTarget ?? invocation.Method;
            //被拦截的方法无返回或者返回类型是Task，那么就直接执行方法（注意Task和Task<>是不同的）
            if (method.ReturnType == typeof(void) || method.ReturnType == typeof(Task))
            {
                invocation.Proceed();//执行被拦截的方法
                return;
            }
            var methodAttributes = method.GetCustomAttributes(true);
            if (methodAttributes.FirstOrDefault(d => d.GetType() == typeof(GetCacheByIdAttribute)) is GetCacheByIdAttribute cacheByIdAttribute)
            {
                var isAsyncMethod = typeof(Task).IsAssignableFrom(method.ReturnType);//等同于 method.ReturnType == typeof(Task) || (method.ReturnType.IsGenericType && method.ReturnType.GetGenericTypeDefinition() == typeof(Task<>));
                var returnType = isAsyncMethod ? method.ReturnType.GenericTypeArguments.FirstOrDefault() : method.ReturnType;
                var name = returnType.Name;
                var id = invocation.Arguments.FirstOrDefault();
                var cacheKey = $"{name}:{id}";
                var cache = _redis.Get<string>(cacheKey);
                if (cache != null)
                {
                    dynamic result = JsonConvert.DeserializeObject(cache, returnType);
                    invocation.ReturnValue = isAsyncMethod ? Task.FromResult(result) : result;
                    return;
                }
                //Console.WriteLine("调用前");
                invocation.Proceed();//执行被拦截的方法
                //Console.WriteLine("调用后");
                #region 通过反射执行方法，这里其实不用再执行一次
                //if (method.IsGenericMethodDefinition)//泛型方法
                //{
                //    invocation.ReturnValue = method.MakeGenericMethod(method.ReturnType.GetGenericArguments()).Invoke(invocation.InvocationTarget, invocation.Arguments);
                //}
                //else
                //{
                //    invocation.ReturnValue = method.Invoke(invocation.InvocationTarget, invocation.Arguments);
                //}
                #endregion
                if (isAsyncMethod)
                {
                    var resultProperty = method.ReturnType.GetProperty("Result");
                    var result = resultProperty.GetValue(invocation.ReturnValue);
                    cache = JsonConvert.SerializeObject(result);
                }
                else
                {
                    cache = JsonConvert.SerializeObject(invocation.ReturnValue);
                }
                if (!string.IsNullOrWhiteSpace(cache)) _redis.SetAsync(cacheKey, cache, cacheByIdAttribute.ExpireSeconds > 0 ? cacheByIdAttribute.ExpireSeconds : -1);
            }
            else
            {
                invocation.Proceed();//执行被拦截的方法
            }
        }
    }
}
